ENV_FILE=/app/src/environments/environment.ts
sed -i "s|\(^\s*GAME_HOST:\s*\).*|\1'$GAME_HOST',|g" $ENV_FILE
sed -i "s|\(^\s*DASHBOARD_BACKEND_URL:\s*\).*|\1'$DASHBOARD_BACKEND_URL',|g" $ENV_FILE
sed -i "s|\(^\s*SCOREBOARD_URL:\s*\).*|\1'$SCOREBOARD_URL',|g" $ENV_FILE
sed -i "s|\(^\s*DASHBOARD_PLAYER_API_URL:\s*\).*|\1'$DASHBOARD_PLAYER_API_URL',|g" $ENV_FILE
sed -i "s|\(^\s*IS_KUBERNETES:\s*\).*|\1$IS_KUBERNETES,|g" $ENV_FILE
echo "" >> $ENV_FILE
