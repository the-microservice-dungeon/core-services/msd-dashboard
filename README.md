# MSD-Dashboard

## Mission and Vision

One recurring issue with playing the Microservice Dungeon game was that while players could compete against each other, nobody really knew what was happening at any given moment. They didn't know which player was dominating or getting smashed by others unless they painstakingly compiled logs themselves. To address this problem, we introduced the Dashboard—a tool designed to observe, analyze, and provide real-time insights into gameplay, enabling users to track and evaluate live events more effectively.

The Dashboard serves multiple purposes. Firstly, it significantly aids in player development by allowing easy creation, starting, and stopping of games. Users can also effortlessly create opponents (custom player) and, after fulfilling certain conditions, even compete against their own player. These features are invaluable for testing and refining player strategies. Secondly, the Dashboard enhances larger code-fights where tracking numerous players becomes challenging. It provides a comprehensive view of the game's status and ongoing events, which would otherwise be difficult to monitor.

From the start of a game, the Dashboard offers real-time monitoring through a live map displaying robots, resources, and detailed information about participating players and planets. Additionally, it presents statistics both graphically and textually, some of which remain accessible even after the game ends for comprehensive analysis.

## Table of Contents
1. [Architecture and Operation of the Dashboard](#architecture-and-operation-of-the-dashboard)
   1. [Architecture and General Function](#architecture-and-general-function)
      1. [What is Angular?](#what-is-angular)
      2. [External Services](#external-services)
      3. [Internal Services](#internal-services)
   2. [How Are Information and Changes Calculated from This Data?](#how-are-information-and-changes-calculated-from-this-data)
   3. [How Does the Custom Player Feature work?](#how-does-the-custom-player-feature-work)
2. [Setup Guide - For Developers](#setup-guide---for-developers)
   1. [Prerequisites](#prerequisites)
   2. [Local Setup](#local-setup)
      1. [Step 1: Prepare the Directory](#step-1-prepare-the-directory)
      2. [Step 2: Clone the Repository](#step-2-clone-the-repository)
      3. [Step 3: Navigate to the Repository Folder](#step-3-navigate-to-the-repository-folder)
      4. [Step 4: Install Dependencies](#step-4-install-dependencies)
      5. [Step 5: Run the Application](#step-5-run-the-application)
   3. [Docker Container Setup](#docker-container-setup)
      1. [Step 1: Clone the Repository](#step-1-clone-the-repository)
      2. [Step 2: Navigate to the Repository Folder](#step-2-navigate-to-the-repository-folder)
      3. [Step 3: Docker Container Setup](#step-3-docker-container-setup)
   4. [Usage](#usage)
   5. [Troubleshooting](#troubleshooting)
3. [Player-Guide: Getting Started](#player-guide-getting-started)
   1. [Player-Guide: How to Play Against Your Own Player](#player-guide-how-to-play-against-your-own-player)
      1. [Requirements](#requirements)
         1. [1. Docker Image](#1-docker-image)
         2. [2. Environment Variables](#2-environment-variables)
      2. [Adding Your Player to the Game](#adding-your-player-to-the-game)
         1. [Adding a Configuration to Your Player](#adding-a-configuration-to-your-player)
      3. [Playing Against Standard Players](#playing-against-standard-players)
4. [Further Instructions for Use](#further-instructions-for-use)
5. [FAQ](#faq)
6. [How to Report Bugs](#how-to-report-bugs)

## Architecture and Operation of the Dashboard

### Architecture and General Function

The Dashboard is built using **Angular**, a popular open-source web application framework developed by Google. Angular provides a robust platform for building dynamic single-page applications (SPAs) with a rich user interface.

#### What is Angular?

Angular is a TypeScript-based open-source framework for building web applications. It extends HTML with additional attributes and binds data to HTML with powerful templating. Angular is known for its speed, performance, and ease of development, making it a preferred choice for modern web applications.

#### External Services

In addition to Angular, the Dashboard relies on several external services that provide endpoints for fetching game data. These services include:

- Game Service: The primary service for game-related operations.
- MSD-Dashboard-Backend: Returns information about all robots and planets currently present.
- Gamelog Service: Provides scoreboards, map data, and makes it possible to map player names to player IDs.
- MSD-Dashboard-Docker-API: Tailored to the Dashboard's needs, it starts Docker containers with specific configurations to enable the implementation of custom players.

#### Internal Services

Within the Dashboard, the central service, Match-Data-Fetch-Service, is responsible for data collection. This service operates as follows:

- Regular Data Fetching: The Match-Data-Fetch-Service calls the data-fetching methods of the respective services at regular intervals, typically three times per game round.
- HTTP Requests: These methods execute HTTP requests to the external service endpoints.
- Data Aggregation: The results from these requests are passed back to the Match-Data-Fetch-Service.
- Data Distribution: The collected data is made available to all other internal services.

<div style="text-align: left;">
  <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/simpleArchitecture.png" style="height: 500px; width: auto" alt="Architecture of the Dashboard, showing how external and internal services are connected">
</div>

##### Key Considerations:

- Real-Time Data Retrieval: Since external services/APIs only provide data for the current round and do not store historical data, the Dashboard must fetch data each round to ensure a comprehensive view of the game.
- Data Consistency: Regular and timely data fetching is crucial for maintaining accurate and complete game data within the Dashboard.

### How Are Information and Changes Calculated from This Data?

To provide comprehensive game data, information on players, robots, and planets is collected for each round. These datasets are temporarily stored and further processed for detailed analysis.

The Match-Data-Service handles this processing by:

- Data Comparison: Comparing the current round’s data with the previous round’s data, focusing on robots.
- Change Detection: Identifying new robots, killed robots, purchased upgrades, and calculating financial transactions such as money earned from selling resources and purchasing robots and upgrades.

Data Persistence and Usage:

- Robot Data: The raw data and derived information are persisted and utilized by various services for further analysis and functionality.
- Planet Data: While planet data is also stored and used for the live map, it does not require the same level of detailed comparison and analysis as robot data.

### How Does the Custom Player Feature Work?

The custom player feature allows players to run as Docker containers on the local machine. Here’s how it works:

1. Player Data Creation:

   - Create data for the player, including the name, email, and Docker image to be used.


2. Configuration File Creation:

   - Generate a configuration file for each custom player, stored in JSON format.
   - This file includes essential environment variables such as player email and player name, which must be unique across all players, and other user-defined configuration variables.
   - The Dashboard automatically creates (if not specified) and updates the configuration file.


3. Container Creation and Launch:

   - The internal 'Docker-Api-Service' sends an HTTP request to the external 'MSD-Dashboard-Docker-Api'.
   - The API uses the provided information (container name, image name, port, and configuration file) to create and start the container.
   - The variables in the configuration file are set as environment variables of the container
   - The API utilizes the Node.js library 'Dockerode' to interface with the Docker engine and manage the container lifecycle.
   - The 'MSD-Dashboard-Docker-API' provides feedback on the success of the container creation and start-up process.


Similarly, the MSD-Dashboard-Docker-Api provides endpoints to stop and delete containers. At the end of each game, all containers are stopped and deleted.

## Setup Guide - For Developers

Welcome to the setup guide for developers. This will walk you through the steps required to clone the repository and get the Dashboard running on your machine.

### Prerequisites

Before you begin, ensure that you have the following installed on your system:
- [Git](https://git-scm.com/downloads)
- [Node.js](https://nodejs.org/en/download/)
- [Angular CLI](https://angular.io/cli)
- [A Running Local Development Environment](https://gitlab.com/the-microservice-dungeon/devops-team/local-dev-environment)
- [Backend](https://gitlab.com/debuas1999/rs-microservice-dungeon-map-api-backend)
- [Gamelog](https://gitlab.com/the-microservice-dungeon/core-services/gamelog)

**Important:** It is crucial to have the local development environment, including the dashboard-backend, the dashboard-docker-api and the gamelog up and running for the Dashboard to function correctly. Please follow the steps provided in their respective links to set up these components before proceeding.

### Local Setup

#### Step 1: Prepare the Directory
First, you need to create or navigate to the directory where you want to clone the repository. Open your terminal or command prompt and use the `cd` command to navigate to your desired directory.

#### Step 2: Clone the Repository
Run the following command in your terminal to clone the repository:

```
git clone https://github.com/MaikRoth/msd-dashboard.git
```

This will create a copy of the repository in your current directory.

#### Step 3: Navigate to the Repository Folder
Once the repository is cloned, navigate into the repository folder by running:

```
cd msd-dashboard
```

Replace `msd-dashboard` with the correct folder name if it's different.

#### Step 4: Install Dependencies
In the repository folder, run the following command to install all the necessary dependencies:

```
npm install
```

This command will download and install all the required Node.js packages.

#### Step 5: Run the Application
Finally, to start the Dashboard, run:

```
ng serve
```

This will start the Angular development server and the Dashboard should be accessible at `http://localhost:4200`.

### Docker Container Setup

#### Step 1: Clone the Repository
Follow the same steps as in the local setup to clone the repository.

#### Step 2: Navigate to the Repository Folder

```
cd msd-dashboard
```

#### Step 3: Docker Container Setup
In Powershell, set up the Docker container by running:

```
docker-compose up
```

This command will create and start the necessary Docker containers.

### Usage

After completing the installation, you can access the Dashboard by navigating to `http://localhost:4200` in your web browser.

### Troubleshooting

If you encounter any issues during the setup, make sure all prerequisites are correctly installed and that you're following the steps in the correct order.


## Player-Guide: Getting Started

If you use the local development environment, the dashboard should be available at localhost:4200. It will navigate you to the 'Control Panel' tab. Here, you can:

- Create a game
- Customize it
- Add players
- Start the game

After starting a game, you will be automatically navigated to the map. It takes a few rounds (usually until round 3) to finish loading. From there, you can start exploring the application and manually stop the game if needed. The data seen in the match statistics tab is available even after stopping the game, but it will be deleted when you create a new game.

### Player-Guide: How to Play Against Your Own Player

The Dashboard allows you to compete against your own player or other custom players. Here’s how it works:

- Creates a Docker container from the Docker image of the player on your local machine.
- Overrides all important variables (e.g., player name, player email, game service URL).
- The player runs in the Docker container and joins the game automatically.
- You can add more than one instance of a specific player to your game.

#### Requirements

To play against your own player, your player needs to fulfill certain requirements.

##### 1. Docker Image

You must provide the Docker image of your player. You can do this by either:

- Adding it to our microservice-dungeon registry: registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry
- Having the image on your local machine or any other registry.

##### 2. Environment Variables

Your player must read and set certain variables from environment variables. This is important because the dashboard needs to change the values of certain variables to start the player correctly as a container. The following environment/system variables need to be implemented in your player with the exact same names:

```
PLAYER_NAME
PLAYER_EMAIL
GAME_HOST
RABBITMQ_HOST
```

**Important:** Please make sure to name these exactly as written here.

Other not required, but potentially necessary variables in some cases:

```
RABBITMQ_USERNAME
RABBITMQ_PASSWORD
RABBITMQ_PORT
```

#### Adding Your Player to the Game

After fulfilling the requirements, visit the dashboard interface at localhost:4200 and start a game via the interface. The following steps explain how to add players to the game.

1. Open Menu:

- Click the 'Add Custom Player' button.
- Click the 'Select Own Player' button. A menu will open where you must enter the details of the player you want to add.

 <div style="text-align: left; margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/addCustomPlayer.png" style="margin-right: 10px; height: 50px; width: auto" alt="Blue button">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/selectOwnPlayer.png" style="height: 50px; width: auto" alt="Light grey button">
 </div>

2. Enter Image Registry:

- Insert the registry of your image if it is in one.
- The default input is the microservice-dungeon registry. If your player is registered there, you don’t need to change anything in this line.
- If the Docker image of your player is on your local machine, leave the input field empty.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/registry.png" style="height: 50px; width: auto" alt="Text box with an default entry and description text above">
 </div>

3. Enter Image Name:

- Insert the name of your Docker image. If the image is in the microservice-dungeon registry, the name is usually something like player-hackschnitzel.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/imageName.png" style="height: 50px; width: auto" alt="Empty text box with a description text above">
 </div>

4. Enter Image Tag:

- Insert the tag of the Docker image. The default input is latest, so you can leave it as is unless you want the image with a specific tag.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/imageTag.png" style="height: 50px; width: auto" alt="Text box with an default entry and description text above">
 </div>

5. Provide Port:

- Provide a port to map the container port to the same port on the host machine (port:port).
- Leaving the field empty or set to 0 will result in a random port assignment (this should be fixed in the future in the Docker API to avoid port assignment when no value is provided).

 <div style="text-align: left;margin-bottom: 30px;">
  <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/port.png" style="height: 50px; width: auto" alt="Empty numeric input field with a description above">
 </div>

##### Adding a Configuration to Your Player

After entering the details of your player image, the Dashboard will ask if you want to add configurations. This allows you to pass additional environment/system variables to your player for further customization. For example, you could have an environment variable named 'STRATEGY' to change the strategy of your player based on the given input. This allows you to start your player with different strategies. If you don’t have any configurations to add, just press 'No, continue without'.

If you decide to add a configuration, a file picker will open. The file you select must be a text file with a single JSON object in it. The file name is not important. It could look like this:

```
{
  "STRATEGY": "aggressive",
  "port": 43553,
  "MAX_NUMBER_ROBOTS": 100
}
```

#### Playing Against Standard Players

For this feature, you don't need any special requirements. You can simply add one or more of the standard players to your game. Just press the 'Add Custom Player' button and then click on their name. Standard players cannot be configured.

**Important:** It might take some time to pull the Docker images for the first time.

## Further Instructions for Use

- Dashboard Usage: Ensure that the Dashboard remains in the foreground at all times. Switching browser tabs or using other applications may disrupt regular data fetching, leading to incomplete game data on the Dashboard (hopefully this can be fixed in the future).

- Game Spectating: When spectating a game, start observing from the beginning (in case you ever intend to start a game through other sources than the dashboard). This ensures accurate data calculations, especially for metrics like player 'balance', which rely on complete game data.

## FAQ

How do I play against my own player?

- [How to play against your own player](#player-guide-how-to-play-against-your-own-player)

Why does the Dashboard show different values than those logged in my player?

- The Dashboard retrieves and calculates game data by fetching it from an API backend, which provides the current state of all robots and planets. The Dashboard continuously fetches this data, manually assigns round numbers, and calculates changes between rounds. Occasionally, specific information may be lost or assigned to incorrect round numbers, leading to discrepancies.

When creating a game with 'previous' settings, will the custom players retain the old configuration, or do I need to provide a new configuration file?

- Custom players will retain the exact configuration provided in the last game. You do not need to provide a new configuration file unless you intend to make changes. Currently, there is no option to see IF a configuration file was provided or not

## How to Report Bugs

The preferred method for reporting bugs is to create an issue on gitlab and provide a detailed description of the problem.

If you encounter any difficulties, you can also message me directly via Discord: bronzescrub or use the appropriate Discord channels on the ArchiLab Discord server.

## Authors

- [Maik Roth](https://gitlab.com/MaikRoth)
- [Florian Lemmer](https://gitlab.com/florianlemmer)
