# Summary

Date : 2023-12-24 19:53:10

Directory c:\\Users\\maikr\\Desktop\\PP\\MSD\\msd-dashboard

Total : 68 files,  17380 codes, 25 comments, 518 blanks, all 17923 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| JSON | 5 | 13,471 | 2 | 5 | 13,478 |
| TypeScript | 30 | 2,084 | 3 | 264 | 2,351 |
| CSS | 14 | 1,003 | 7 | 169 | 1,179 |
| HTML | 14 | 693 | 13 | 35 | 741 |
| Markdown | 1 | 69 | 0 | 31 | 100 |
| JSON with Comments | 1 | 25 | 0 | 1 | 26 |
| YAML | 2 | 25 | 0 | 3 | 28 |
| Docker | 1 | 10 | 0 | 10 | 20 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 68 | 17,380 | 25 | 518 | 17,923 |
| . (Files) | 10 | 13,600 | 2 | 50 | 13,652 |
| src | 58 | 3,780 | 23 | 468 | 4,271 |
| src (Files) | 3 | 27 | 2 | 8 | 37 |
| src\\app | 55 | 3,753 | 21 | 460 | 4,234 |
| src\\app (Files) | 12 | 546 | 0 | 58 | 604 |
| src\\app\\controlpanel | 9 | 460 | 4 | 70 | 534 |
| src\\app\\controlpanel (Files) | 3 | 167 | 4 | 24 | 195 |
| src\\app\\controlpanel\\gameshandler | 6 | 293 | 0 | 46 | 339 |
| src\\app\\controlpanel\\gameshandler (Files) | 3 | 57 | 0 | 7 | 64 |
| src\\app\\controlpanel\\gameshandler\\game | 3 | 236 | 0 | 39 | 275 |
| src\\app\\header | 3 | 69 | 0 | 11 | 80 |
| src\\app\\map | 18 | 1,967 | 13 | 226 | 2,206 |
| src\\app\\map (Files) | 3 | 519 | 3 | 55 | 577 |
| src\\app\\map\\planet | 3 | 387 | 1 | 45 | 433 |
| src\\app\\map\\player | 3 | 297 | 0 | 42 | 339 |
| src\\app\\map\\robot | 3 | 526 | 0 | 46 | 572 |
| src\\app\\map\\settingsbar | 3 | 151 | 9 | 25 | 185 |
| src\\app\\map\\sidebar | 3 | 87 | 0 | 13 | 100 |
| src\\app\\scoreboard | 4 | 322 | 4 | 42 | 368 |
| src\\app\\shared | 5 | 135 | 0 | 25 | 160 |
| src\\app\\shared (Files) | 2 | 104 | 0 | 18 | 122 |
| src\\app\\shared\\chart | 3 | 31 | 0 | 7 | 38 |
| src\\app\\store | 4 | 254 | 0 | 28 | 282 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)