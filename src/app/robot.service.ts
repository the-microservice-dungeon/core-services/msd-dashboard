import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Robot } from "./map/robot/robot.component";
import { PlayerService } from "./player.service";
import {catchError, map, Observable, of} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
    providedIn: 'root'
})
export class RobotsService {

  private robotsEndpoint = environment.DASHBOARD_BACKEND_URL
    ? (environment.DASHBOARD_BACKEND_URL + '/robots')
    : "http://localhost:8096/robots";
    constructor(
        private http: HttpClient,
        private playerService : PlayerService) { }

    robots: Robot[] = [];
    highlightedRobots: Robot[] = [];

    fetchRobots() {
        this.http.get(this.robotsEndpoint).subscribe({
            next: (res: any[]) => {
                const updatedRobots = res.map(robotData => this.mapToRobot(robotData));
                let newRobotsArray: Robot[] = [];
                const highlightedPlayerIds = this.playerService.getHighlightedPlayerIds()
                updatedRobots.forEach(newRobot => {
                    const existingRobot = this.robots.find(r => r.robotId === newRobot.robotId);
                    if (existingRobot) {
                        if(existingRobot.img[1] === "") {
                          existingRobot.img = [`https://robohash.org/${existingRobot.name}.png`,
                            this.getTeamRobotImage(this.playerService.getPlayerColor(newRobot.playerId))];
                        }
                        newRobotsArray.push({ ...existingRobot, ...newRobot });
                    } else {
                        const robotName = newRobot.robotId.substring(0,5);
                        newRobotsArray.push({
                            ...newRobot,
                            highlighted: highlightedPlayerIds.some(id => id === newRobot.playerId),
                            name: robotName,
                            img: [`https://robohash.org/${robotName}.png`, this.getTeamRobotImage(this.playerService.getPlayerColor(newRobot.playerId))],
                            alive: true
                        });
                        if (highlightedPlayerIds.some(id => id === newRobot.playerId)){
                            this.highlightedRobots.push(newRobot)
                        }
                    }
                });
                this.robots = newRobotsArray;
            },
            error: (error) => {
                console.error('Error fetching robots:', error);
            }
        });
    }

    fetchChangeReturnRobots():Observable<Robot[]> {
      return this.http.get<any[]>(this.robotsEndpoint).pipe(
        map((res: any[]) => {
          const updatedRobots = res.map(robotData => this.mapToRobot(robotData));
          console.log("number robots"+ updatedRobots.length);
          let newRobotsArray: Robot[] = [];
          const highlightedPlayerIds = this.playerService.getHighlightedPlayerIds()
          updatedRobots.forEach(newRobot => {
            const existingRobot = this.robots.find(r => r.robotId === newRobot.robotId);
            if (existingRobot) {
              if(existingRobot.img[1] === "") {
                existingRobot.img[1] = this.getTeamRobotImage(this.playerService.getPlayerColor(newRobot.playerId));
              }
              newRobotsArray.push({ ...existingRobot, ...newRobot });
            } else {
              const robotName = newRobot.robotId.substring(0,5);
              newRobotsArray.push({
                ...newRobot,
                highlighted: highlightedPlayerIds.some(id => id === newRobot.playerId),
                name: robotName,
                img: [`https://robohash.org/${robotName}.png`, this.getTeamRobotImage(this.playerService.getPlayerColor(newRobot.playerId))],
                alive: true
              });
              if (highlightedPlayerIds.some(id => id === newRobot.playerId)){
                this.highlightedRobots.push(newRobot)
              }
            }
          });
          this.robots = newRobotsArray;

          this.robots.forEach(robot => {
            this.highlightedRobots.forEach(hlRobot => {
              if (hlRobot.robotId === robot.robotId) {
                robot.highlighted = true;
              }
            })
          })

          return newRobotsArray;
        }),
        catchError(error => {
          console.error('Error fetching robots:', error);
          return of([]);
        })
      );
    }

    fetchAndReturnRobots(): Observable<Robot[]> {
      return this.http.get<any[]>(this.robotsEndpoint).pipe(
        map((res: any[]) => {
          return res.map(robotData => this.mapToRobot(robotData));
        }),
        catchError(error => {
          console.error('Error fetching robots:', error);
          return of([]);
        })
      );
    }
    getTeamRobotImage(color: string) {
      if(color === undefined)
        return ""
      const colorLowerCase = color.toLowerCase();
      const imageName = `${colorLowerCase}-robot.png`;
      return `../../../assets/images/robots/${imageName}`;
      }
    getRobots() {
        this.fetchRobots();
        this.robots.forEach(robot => {
            this.highlightedRobots.forEach(hlRobot => {
                if (hlRobot.robotId === robot.robotId) {
                    robot.highlighted = true;
                }
            })
        })
        return this.robots;
    }

    highlightRobot(robot: Robot) {
        this.highlightedRobots.push(robot);
    }
    highlightRobotByName(name: string) {
        const robotToHighlight = this.robots.find(robot => robot.name === name);

        if (robotToHighlight && !this.highlightedRobots.includes(robotToHighlight)) {
            this.highlightedRobots.push(robotToHighlight);
        }
    }
    resetHighlightRobot(robot: Robot) {
        this.highlightedRobots = this.highlightedRobots.filter(r => r.robotId !== robot.robotId);
    }
    resetHighlightRobotByName(name: string) {
        const robotToUnhighlight = this.robots.find(robot => robot.name === name);
        this.highlightedRobots = this.highlightedRobots.filter(r => r.robotId !== robotToUnhighlight.robotId);
    }
    highlightRobotsOfPlayer(playerId: string) {
        this.robots.forEach(robot => {
            if (robot.playerId == playerId) {
                this.highlightedRobots.push(robot);
            }
        })
        this.playerService.addPlayerIdToHighlightedPlayerIds(playerId)
    }

    resetHighlightOfPlayer(playerId: string) {
        this.highlightedRobots = this.highlightedRobots.filter(r => r.playerId !== playerId);
        this.playerService.setHighlightedPlayerIds(this.playerService.getHighlightedPlayerIds().filter(id => id !== playerId));
    }

    areAttributesEqual(robot1: Robot, robot2: Robot): boolean {
        return (
            robot1.planetId === robot2.planetId &&
            robot1.playerId === robot2.playerId &&
            robot1.robotId === robot2.robotId &&
            robot1.vitals.health === robot2.vitals.health &&
            robot1.vitals.energy === robot2.vitals.energy &&
            robot1.cargo.capacity === robot2.cargo.capacity &&
            robot1.cargo.free === robot2.cargo.free &&
            robot1.cargo.used === robot2.cargo.used &&
            robot1.cargo.coal === robot2.cargo.coal &&
            robot1.cargo.iron === robot2.cargo.iron &&
            robot1.cargo.gem === robot2.cargo.gem &&
            robot1.cargo.gold === robot2.cargo.gold &&
            robot1.cargo.platin === robot2.cargo.platin &&
            robot1.highlighted === robot2.highlighted &&
            robot1.levels.damage === robot2.levels.damage &&
            robot1.levels.energy === robot2.levels.energy &&
            robot1.levels.energyRegeneration === robot2.levels.energyRegeneration &&
            robot1.levels.health === robot2.levels.health &&
            robot1.levels.miningLevel === robot2.levels.miningLevel &&
            robot1.levels.miningSpeed === robot2.levels.miningSpeed &&
            robot1.levels.storage === robot2.levels.storage
        );
    }

    mapToRobot(obj: any) {
        return {
            robotId: obj.robotId,
            planetId: obj.planetId,
            playerId: obj.playerId,
            vitals: obj.vitals,
            levels: obj.levels,
            cargo: obj.cargo,
            highlighted: false,
            alive: true
        };
    }

    resetAll(){
      this.robots = [];
      this.highlightedRobots = [];
    }
}
