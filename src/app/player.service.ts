import { ElementRef, EventEmitter, Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Player } from './map/sidebar/player/player.component';
import {switchMap, of, Observable, map, forkJoin, catchError, tap} from "rxjs";
import { MoneyService } from "./money.service";
import { Robot } from "./map/robot/robot.component";
import {environment} from "../environments/environment";

export type TransactionEntry = {
  type: 'BUY' | 'SELL' | 'INIT',
  entity: string,
  item: string,
  amount: number
  earning: number
};
@Injectable({ providedIn: 'root' })
export class PlayerService {

  private transactionHistory = new Map<string, TransactionEntry[]>();
  private teamColors = [ 'blue', 'green', 'orange', 'purple', 'red', 'yellow',
    'gray', 'silver', 'pink', 'white', 'aqua',
    'magenta', 'lime', 'teal', 'olive', 'maroon',
    'navy', 'gold', 'peachpuff', 'saddlebrown'];//'lightblue' //'black' schrift nicht erkenntbar
  private playerColorsMap = new Map<string, string>();
  private highlightedPlayerIds : string[] = [];

  private scoreboardUrl = environment.SCOREBOARD_URL || 'http://localhost:8089';
  private gameUrl = environment.GAME_HOST || 'http://localhost:8080';

  constructor(
    private http: HttpClient,
    private moneyService: MoneyService) {
    this.loadTransactionHistoryFromLocalStorage();
  }

  addTransaction(playerId: string, entry: TransactionEntry): void {
    const history = this.transactionHistory.get(playerId) || [];
    history.push(entry);
    this.transactionHistory.set(playerId, history);
    this.saveTransactionHistoryToLocalStorage();
  }
  clearTransactionHistory(): void {
    this.transactionHistory.clear();
    this.saveTransactionHistoryToLocalStorage();
  }
  getPlayerColor(playerId: string): string {
    return this.playerColorsMap.get(playerId);
  }
  getPlayerColorMap(){
    return this.playerColorsMap
  }
  clearColorMap(){
    this.playerColorsMap.clear();
  }
  getUnusedColor(): string {
    const usedColors = new Set(this.playerColorsMap.values());
    const availableColors = this.teamColors.filter(color => !usedColors.has(color));

    if (availableColors.length === 0) {
      throw new Error('No more unique colors available');
    }

    const randomColor = availableColors[Math.floor(Math.random() * availableColors.length)];
    return randomColor;
  }
  setHighlightedPlayerIds(ids : string[]){
    this.highlightedPlayerIds = ids;
  }
  getHighlightedPlayerIds() : string[]{
    return this.highlightedPlayerIds
  }
  addPlayerIdToHighlightedPlayerIds(playerId:string){
    this.highlightedPlayerIds.push(playerId)
  }
  getRandomColor(): string {
    return this.teamColors[Math.floor(Math.random() * this.teamColors.length)];
  }
  getTransactionHistoryOfPlayer(playerId: string): TransactionEntry[] {
    return this.transactionHistory.get(playerId) || [];
  }
  fetchPlayers(): Observable<Player[]> {
    const gamesRequest = this.http.get<any[]>(this.gameUrl + '/games').pipe(
      map(games => games.flatMap(game => game.participatingPlayers))
    );

    const scoreboardRequest = this.http.get<{ scoreboardEntries: any[] }>(this.scoreboardUrl + '/scoreboard');

    return forkJoin([gamesRequest, scoreboardRequest]).pipe(
      map(([playerNames, scoreboard]) => {
        return playerNames.map(name => {
          const playerScoreboard = scoreboard.scoreboardEntries.find(entry => entry.player.name === name);

          if (playerScoreboard && playerScoreboard.player.id) {
            const randomColor = this.getUnusedColor();
            this.playerColorsMap.set(playerScoreboard.player.id, randomColor);
            const newPlayer: Player = {
              playerId: playerScoreboard.player.id,
              name: name,
              robots: [],
              money: 0,
              isHighlighted: false,
              visitedPlanetIds: []
            };
            if(!this.hasPlayerGottenInitMoney(newPlayer.playerId)){
              this.moneyService.addMoney(newPlayer.playerId, 500);
              this.addTransaction(newPlayer.playerId, { type: 'INIT', entity: "MSD", item: 'cash', amount: 1, earning: 500 });
            }
            return newPlayer;
          }
          return null;
        }).filter(player => player !== null);
      })
    );
  }

  resetHighlightOfPlayer(player:Player){
    player.robots.forEach(robot => robot.highlighted = false )
  }
  highlightRobotsOfPlayer(player:Player){
    player.robots.forEach(robot => robot.highlighted = true )
  }
  private saveTransactionHistoryToLocalStorage(): void {
    const serializedHistory = JSON.stringify(Array.from(this.transactionHistory.entries()));
    localStorage.setItem('transactionHistory', serializedHistory);
  }

  private loadTransactionHistoryFromLocalStorage(): void {
    const serializedHistory = localStorage.getItem('transactionHistory');
    if (serializedHistory) {
      const history = new Map<string, TransactionEntry[]>(JSON.parse(serializedHistory));
      this.transactionHistory = history;
    }
  }

  private hasPlayerGottenInitMoney(playerId:string):boolean{
    if (!this.transactionHistory.has(playerId)) {
      return false;
    }
    const transactions = this.transactionHistory.get(playerId);
    return transactions.some(transaction => transaction.type === 'INIT');
  }
}
