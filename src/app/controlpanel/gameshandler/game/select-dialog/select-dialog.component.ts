import {Component, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-select-dialog',
  templateUrl: './select-dialog.component.html',
  styleUrl: './select-dialog.component.css'
})
export class SelectDialogComponent {

  @Output() onConfirmation = new EventEmitter<string>();

  onYesClick(): void {
    this.onConfirmation.emit('yes');
  }

  onNoClick(): void {
    this.onConfirmation.emit('no');
  }

  onCancelClick(): void {
    this.onConfirmation.emit('cancel');
  }
}
