import {Component, ElementRef, Input, OnChanges, OnDestroy, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {GamesService} from '../../../games.service';
import {Router} from '@angular/router';
import {Game} from '../gameshandler.component';
import {MoneyService} from '../../../money.service';
import {PlayerService} from '../../../player.service';
import {catchError, finalize, Observable, of, pipe, tap} from 'rxjs';
import {DockerService} from "../../../docker.service";
import {CustomPlayerService} from "../../../custom-player.service";
import {MatDialog} from '@angular/material/dialog';
import {SelectDialogComponent} from './select-dialog/select-dialog.component';
import {environment} from "../../../../environments/environment";
import {ToastrService} from "ngx-toastr";


export type DockerContainerResponse = {
  message: string;
  containerName?: string;
}


@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrl: './game.component.css',
})
export class GameComponent implements OnInit {
  @Input() game: Game;
  @Input() planets;
  @Input() robots;
  @Input() totalScore;
  @Input() achievements;

  collapsedStates: Map<string, boolean> = new Map();
  duration: number;
  maxRounds: number;
  showCustomPlayerSelection: boolean;
  customPlayerData$: Observable<{ [key: string]: string }>;
  selectedPlayerName: string;
  selectedImage: string;
  isPlayerSelectionInProgress: boolean;
  showOwnPlayerInputField:boolean;
  showSpinner:boolean;
  isOnCluster: boolean = environment.IS_KUBERNETES;


  @ViewChild('fileInput') fileInput: any;
  userInputRegistry: string = "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/";
  userInputImage: string = "";
  userInputTag: string = "latest";

  constructor(
    private gamesService: GamesService,
    private router: Router,
    private moneyService: MoneyService,
    private dockerService: DockerService,
    private customPlayerService: CustomPlayerService,
    private playerService: PlayerService,
    public dialog: MatDialog,
    private toastr: ToastrService,
  ) {
  }

  ngOnInit() {
    this.initializeCollapsedState();
    this.customPlayerData$ = this.customPlayerService.playerImageData$;
    this.showCustomPlayerSelection = false;
    this.isPlayerSelectionInProgress = false;
    this.showOwnPlayerInputField = false;
    this.showSpinner = false;
  }

  initializeCollapsedState() {
    const collapsedState = this.getCollapsedStateFromLocalStorage(this.game.gameId);
    this.collapsedStates.set(this.game.gameId, collapsedState);
  }

  getCollapsedStateFromLocalStorage(gameId: string) {
    return localStorage.getItem(`game-collapsed-${gameId}`) === 'true';
  }

  setCollapsedStateInLocalStorage(gameId: string, isCollapsed: boolean) {
    localStorage.setItem(`game-collapsed-${gameId}`, String(isCollapsed));
  }

  toggleCollapse(gameId: string) {
    const currentCollapsedState = this.collapsedStates.get(gameId) || false;
    const newCollapsedState = !currentCollapsedState;
    this.collapsedStates.set(gameId, newCollapsedState);
    this.setCollapsedStateInLocalStorage(gameId, newCollapsedState);
  }

  onStartGame(id: string) {
    this.gamesService.startGame(id).subscribe(() => {
      this.redirectToMap();
    })
    this.moneyService.clear();
    this.playerService.clearColorMap();
    this.playerService.clearTransactionHistory();
  }
  updateRoundTime() {
    const params = {
      duration: this.duration
    }
    this.gamesService.updateRoundTime(this.game.gameId, params).subscribe(() => {})
  }
  updateMaxRounds() {
    const params = {
      maxRounds: this.maxRounds
    }
    this.gamesService.updateMaxRounds(this.game.gameId, params).subscribe(() => {})
  }
  redirectToMap() {
    this.router.navigate(['/map']);
  }

  redirectToControlPanel() {
    this.router.navigate(['/controlpanel'])
  }

  onEndGame(id: string) {
    this.gamesService.endGame(id).subscribe(() => {
      this.redirectToControlPanel()
    })
    this.moneyService.clear()
    this.playerService.clearColorMap();
    this.playerService.clearTransactionHistory();
    this.customPlayerService.stopAllCustomPlayerContainersByGameId(this.game.gameId);
    //this.customPlayerService.clearAll();
  }

  handleClickedPlayerName(playerName:string){
    let image:string;
    this.customPlayerData$.pipe().subscribe((data) => {
      image = data[playerName];
    });
    this.openConfigurationSelectDialog(playerName, image);
  }


  openConfigurationSelectDialog(playerName:string, image: string): void {
    if(this.isPlayerSelectionInProgress == false) {
      this.disableCustomPlayerSelection()
      this.selectedPlayerName = playerName;
      this.selectedImage = image;
      const dialogRef = this.dialog.open(SelectDialogComponent, {
        panelClass: 'config-dialog',
      });

      dialogRef.afterClosed().subscribe(result => {
        if (result === 'yes') {
          this.openFilePicker();
        } else if (result === 'no') {
          this.noConfigFileSelected();
        } else {
          this.enableCustomPlayerSelection();
        }
      });
    }
  }

  openFilePicker() {
    const fileInput = document.createElement('input');
    fileInput.type = 'file';
    fileInput.style.visibility = 'hidden';
    fileInput.addEventListener('change', (event) => this.onConfigFileSelected(event));
    fileInput.addEventListener('cancel', () => {this.enableCustomPlayerSelection();});
    fileInput.click();
  }


  onConfigFileSelected(event: any) {
    const playerName = this.selectedPlayerName;
    const image = this.selectedImage;
    const selectedFile = event.target.files[0];
    this.customPlayerService.createCustomPlayerBy(playerName,image, selectedFile,this.game.gameId)
      .then(customPlayer => {
        console.log(customPlayer);
        this.startDockerContainer(customPlayer.imageName, customPlayer.port, customPlayer.name, customPlayer.configFile);
      })
      .catch(error => {
        console.error('Error:', error);
        this.enableCustomPlayerSelection();
        this.disableOwnPlayerInputField();
        //hier vlt notification aussenden?
      });
  }

  noConfigFileSelected(){
    const playerName = this.selectedPlayerName;
    const image = this.selectedImage;
    this.customPlayerService.createCustomPlayerBy(playerName,image, null, this.game.gameId)
      .then(customPlayer => {
        console.log(customPlayer);
        this.startDockerContainer(customPlayer.imageName, customPlayer.port, customPlayer.name, customPlayer.configFile);
      })
      .catch(error => {
        console.error('Error:', error);
        this.enableCustomPlayerSelection();
        this.disableOwnPlayerInputField();
        //hier vlt notification aussenden?
      });
  }

  startDockerContainer(imageName:string, port:number, containerName:string, configFile:any){
    this.toggleShowSpinner();
    this.dockerService.startDockerContainerWithConfigFile(imageName, port, containerName, configFile)
      .pipe(
        tap((res: DockerContainerResponse) => {
          console.log(`Container started successfully: ${res.containerName}`);
          this.toastr.success('Player created successfully!', '');
        }),
        catchError(error => {
          console.error('Error calling API:', error);
          this.customPlayerService.deleteCustomPlayerByName(containerName);

          let userFriendlyMessage = 'An unknown error occurred.';
          const errorMessage = error.error && error.error.message ? error.error.message : '';
          if (errorMessage.includes('manifest unknown')) {
            userFriendlyMessage = 'The specified Docker image could not be found. Please check the image name, registry and tag.';
          } else if (errorMessage.includes('requested access to the resource is denied')) {
            userFriendlyMessage = `The specified Docker image could not be accessed or found. Please check the image name, registry and tag.`;
          } else if (errorMessage.includes('access forbidden')) {
            userFriendlyMessage = 'The specified Docker image could not be accessed or found. Please check the image name, registry and tag.';
          } else if (errorMessage.includes('no such host')) {
            userFriendlyMessage = `Couldn't find the specified host. Please check the registry url.`;
          } else if (errorMessage.includes('invalid reference format')) {
            userFriendlyMessage = `Couldn't find the specified registry. Please check the registry url.`;
          } else if (errorMessage.includes('The container name') && errorMessage.includes('is already in use by container')) {
            userFriendlyMessage = `This usually shouldn't happen! There already exists a container with this name. Please check your docker for a container named '`+ containerName + `' and delete it.`;
          } else {
            userFriendlyMessage = errorMessage || userFriendlyMessage;
          }
          this.toastr.error(userFriendlyMessage, 'Error occured while creating player!', {
            timeOut: 15000
          });
          return of(null);
        }),
        finalize(() => {
          this.enableCustomPlayerSelection();
          this.disableOwnPlayerInputField();
          this.toggleShowSpinner();
        })
      )
      .subscribe();
  }

  onAddCustomPlayer(){
    if(!this.isPlayerSelectionInProgress){
      this.toggleShowCustomPlayerSelection();
      this.disableOwnPlayerInputField();
    }
  }

  enableCustomPlayerSelection(){
    this.isPlayerSelectionInProgress = false;
  }

  disableCustomPlayerSelection(){
    this.isPlayerSelectionInProgress = true;
  }

  toggleShowCustomPlayerSelection(){
    this.showCustomPlayerSelection = !this.showCustomPlayerSelection
  }

  enableOwnPlayerInputField(){
    this.showOwnPlayerInputField = true;
  }

  disableOwnPlayerInputField(){
    this.showOwnPlayerInputField = false;
    this.resetUserInputs();
  }

  onSelectOwnPlayer(){
    this.enableOwnPlayerInputField();
  }

  onConfirmSelectOwnPlayer(){
    let registry = (document.getElementById('registry') as HTMLInputElement).value.trim();
    let imageName = (document.getElementById('image') as HTMLInputElement).value.trim();
    let tag = (document.getElementById('tag') as HTMLInputElement).value.trim();
    if(imageName !== "" && tag !== ""){
      if(registry !== ""){
        if(!registry.endsWith('/'))
          registry += "/";
      }
      let image = registry+imageName+":"+tag;
      this.openConfigurationSelectDialog(imageName, image);
    }
  }

  onCancelSelectOwnPlayer(){
    if(!this.isPlayerSelectionInProgress)
      this.disableOwnPlayerInputField();
  }

  resetUserInputs(){
    this.userInputRegistry = "registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry/";
    this.userInputImage = "";
    this.userInputTag = "latest";
  }

  toggleShowSpinner(){
    this.showSpinner = !this.showSpinner;
  }

}
