import { Injectable } from '@angular/core';
import {catchError, forkJoin, Observable, of, Subscription, switchMap, takeWhile, tap, timer} from "rxjs";
import {Planet} from "./map/planet/planet.component";
import {Robot} from "./map/robot/robot.component";
import {Player} from "./map/sidebar/player/player.component";
import {Game} from "./controlpanel/gameshandler/gameshandler.component";
import {PlanetService} from "./planet.service";
import {RobotsService} from "./robot.service";
import {GamesService} from "./games.service";
import {PlayerService} from "./player.service";
import {
  saveGamesToLocalStorage
} from "./app.component";
import {CustomPlayerService} from "./custom-player.service";

export interface RobotDatabaseEntry {
  round: number;
  robots: Robot[];
}

export interface PlanetDatabaseEntry {
  round: number;
  planets: Planet[];
}

export interface PlayerDatabaseEntry {
  round: number;
  players: Player[];
}

@Injectable({
  providedIn: 'root'
})
export class MatchDataFetchService {

  private planetSubscription: Subscription;
  private robotSubscription: Subscription;
  private gameSubscription: Subscription;
  private playerSubscription: Subscription;
  private pollPlayerSubscription: Subscription;

  private fetchIntervalTime = 1000;
  private currentRound=0;
  private roundLength= 15000;
  private subscriptionsStarted = false;
  private isServiceReset = false;

  private players: Player[] = [];
  private games: Game[] = [];
  private planetsByRound: { [round: number]: Planet[] } = {};
  private robotsByRound: { [round: number]: Robot[] } = {};
  private playersByRound: { [round: number]: Player[] } = {};

  //hold latest available data about the game+players, even if game already stopped -> to show matchdata after game has finished
  private latestGame: Game;
  private latestParticipatingPlayers: Player[] = [];

  constructor(private planetService: PlanetService,
              private robotService: RobotsService,
              private gamesService: GamesService,
              private playerService: PlayerService,
              private customPlayerService: CustomPlayerService,
              )
  {
    //in match-data-service
    this.loadLatestGameFromLocalStorage();
    this.loadLatestParticipatingPlayersFromLocalStorage();

    this.gameSubscription = timer(0,this.fetchIntervalTime).subscribe(() => this.onGetGames());
  }


  startSubscriptions(){
    this.subscriptionsStarted = true;
    this.planetSubscription = timer(0,this.roundLength/3 ).subscribe(() => this.onGetPlanets(this.currentRound));
    this.robotSubscription = timer(0,this.roundLength/3).subscribe(() => this.onGetRobots(this.currentRound));

    this.pollForPlayers();
  }

  onGetPlanets(currentRound:number){
    this.planetService.fetchAndReturnPlanets().pipe(
      catchError(error => {
        console.error('Error fetching planets:', error);
        return of([]);
      })
    ).subscribe((planets: Planet[]) => {
      console.log("number planets:"+ planets.length)
      this.planetsByRound[currentRound] = planets;
    });
  }

  onGetRobots(currentRound:number){

    this.robotService.fetchChangeReturnRobots().pipe(
      catchError(error => {
        console.error('Error fetching robots:', error);
        return of([]);
      })
    ).subscribe((robots: Robot[]) => {
      this.robotsByRound[currentRound] = robots;
    });

    /*const robotFetch$: Observable<Robot[]> = this.robotService.fetchChangeReturnRobots().pipe(
      catchError(error => {
        console.error('Error fetching robots:', error);
        return of([] as Robot[]); // Explicitly return an empty array of Robot
      })
    );

    const gamesFetch$: Observable<Game[]> = this.gamesService.fetchGames().pipe(
      catchError(error => {
        console.error('Error fetching additional data:', error);
        return of([] as Game[]); // Explicitly return null for AdditionalData
      })
    );

    forkJoin({
      robots: robotFetch$,
      games: gamesFetch$
    }).subscribe(({ robots, games }: { robots: Robot[], games: Game[] | null }) => {
      if(games.length>0){
        this.robotsByRound[games[0].currentRoundNumber] = robots;
      }
    });*/
  }

  onGetGames(){
    this.gamesService.fetchGames().subscribe((games: Game[]) => {
      let stoppedGames: Game[] = this.checkForStoppedGames(this.games, games);
      stoppedGames.forEach(game =>{
        this.customPlayerService.stopAllCustomPlayerContainersByGameId(game.gameId);
      });
      this.games = games;
      saveGamesToLocalStorage(this.games)
      if (this.games.length > 0) {
        this.currentRound = this.games[0].currentRoundNumber;
        this.latestGame = this.games[0];
        this.saveLatestGameToLocalStorage()
        this.roundLength = this.games[0].roundLengthInMillis;
        if(this.games[0].gameStatus === 'started'){
          if(this.subscriptionsStarted === false) {
            this.isServiceReset = false;
            this.startSubscriptions();
          }
        }
        else{
          this.unsubscribeAll();
          this.resetAll();
        }
      }
      else{
        this.onGameEnded();
      }
    })
  }

  onGameEnded(){
    this.unsubscribeAll();
    this.robotService.resetAll();
    localStorage.setItem('game', JSON.stringify(null));
  }

  checkForStoppedGames(oldGames: Game[], newGames: Game[]): Game[]{
    let stoppedGames: Game[] = [];
    oldGames.forEach(game =>{
      if(newGames.find(newGame => game.gameId === newGame.gameId) === undefined){
        stoppedGames.push(game);
      }
    });
    return stoppedGames;
  }

  getCurrentRound(){
    return this.currentRound;
  }

  pollForPlayers() {
    this.pollPlayerSubscription = timer(0, 1000)
      .pipe(
        switchMap(() => this.gamesService.getPlayingPlayers()),
        switchMap((participatingPlayers: string[]) => {
          if (this.players.length !== participatingPlayers.length) {
            return this.playerService.fetchPlayers();
          } else {
            throw new Error('Polling completed');
          }
        }),
        takeWhile(() => true, true)
      )
      .subscribe({
        next: (players: Player[]) => {
          this.players = players;
          if(players.length>0){
            this.latestParticipatingPlayers = players;
            this.saveLatestParticipatingPlayersFromLocalStorage();
          }
        },
        error: (err) => {
        }
      });
  }

  getPlanetsByRound(){
    return this.planetsByRound;
  }

  getPlanetsFromHighestRound(){
    const rounds = Object.keys(this.planetsByRound).map(Number);
    if (rounds.length === 0) {
      return [];
    }
    const highestRound = Math.max(...rounds);
    return this.planetsByRound[highestRound];
  }

  getRobotsByRound(){
    return this.robotsByRound;
  }

  getRobotsFromHighestRound(){
    const rounds = Object.keys(this.robotsByRound).map(Number);
    if (rounds.length === 0) {
      return [];
    }
    const highestRound = Math.max(...rounds);
    return this.robotsByRound[highestRound];
  }

  getPlayersByRound(){
    return this.playersByRound;
  }

  getGames(){
    return this.games;
  }

  getPlayers(){
    return this.players;
  }

  getLatestGame(){
    return this.latestGame;
  }

  getLatestParticipatingPlayers(){
    return this.latestParticipatingPlayers
  }

  saveLatestGameToLocalStorage(){
    localStorage.setItem('latest-game', JSON.stringify(this.latestGame));
  }

  loadLatestGameFromLocalStorage(){
    const savedGame = localStorage.getItem('latest-game');
    this.latestGame = JSON.parse(savedGame);
  }

  saveLatestParticipatingPlayersFromLocalStorage(){
    localStorage.setItem('latest-participating-players', JSON.stringify(this.latestParticipatingPlayers));
  }

  loadLatestParticipatingPlayersFromLocalStorage(){
    const savedPlayers = localStorage.getItem('latest-participating-players');
    this.latestParticipatingPlayers = JSON.parse(savedPlayers);
  }


  unsubscribeAll(){
    if (this.robotSubscription) {
      this.robotSubscription.unsubscribe();
    }
    if (this.planetSubscription) {
      this.planetSubscription.unsubscribe();
    }
    if (this.playerSubscription) {
      this.playerSubscription.unsubscribe();
    }
    if(this.pollPlayerSubscription){
      this.pollPlayerSubscription.unsubscribe();
    }
    this.subscriptionsStarted = false;
  }

  resetAll(){
    if(!this.isServiceReset){
      this.isServiceReset = true;
      this.currentRound=0;
      this.players = [];
      this.games = [];
      this.planetsByRound  = {};
      this.robotsByRound = {};
      this.playersByRound = {};
    }
  }
}
