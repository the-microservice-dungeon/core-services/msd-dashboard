import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { ControlpanelComponent } from './controlpanel/controlpanel.component';
import { HeaderComponent } from './header/header.component';
import { MapComponent } from './map/map.component';
import { ScoreboardComponent } from './scoreboard/scoreboard.component';
import { SidebarComponent } from './map/sidebar/sidebar.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { PlanetComponent } from './map/planet/planet.component';
import { GameComponent } from './controlpanel/gameshandler/game/game.component';
import { PlayerComponent } from './map/sidebar/player/player.component';
import { RobotComponent } from './map/robot/robot.component';
import { SettingsbarComponent } from './map/settingsbar/settingsbar.component';
import { StoreModule } from '@ngrx/store';
import { customGamesReducer, mapReducer } from './store/dashboard.reducer';
import { EffectsModule } from '@ngrx/effects';
import { GameshandlerComponent } from './controlpanel/gameshandler/gameshandler.component';
import { TransactionItemFormatPipe } from './transaction-item-format.pipe';
import { JunkyardComponent } from './map/sidebar/junkyard/junkyard.component';
import { SelectDialogComponent } from './controlpanel/gameshandler/game/select-dialog/select-dialog.component';
import {MatDialogActions, MatDialogClose, MatDialogContent} from "@angular/material/dialog";
import { MatchStatisticsComponent } from './match-statistics/match-statistics.component';
import {NgApexchartsModule} from "ng-apexcharts";
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import {MatInput} from "@angular/material/input";
import {MatButton} from "@angular/material/button";
import { KeysPipe } from './keys.pipe';
import { KeyValuePairPipe } from './key-value-pair.pipe';
import { ToastrModule } from 'ngx-toastr';
import {MatIcon} from "@angular/material/icon";
import { MatchChartComponent } from './match-statistics/match-chart/match-chart.component';




@NgModule({
  declarations: [
    AppComponent,
    ControlpanelComponent,
    HeaderComponent,
    MapComponent,
    ScoreboardComponent,
    SidebarComponent,
    PlanetComponent,
    GameComponent,
    PlayerComponent,
    RobotComponent,
    SettingsbarComponent,
    GameshandlerComponent,
    TransactionItemFormatPipe,
    JunkyardComponent,
    SelectDialogComponent,
    MatchStatisticsComponent,
    KeysPipe,
    KeyValuePairPipe,
    MatchChartComponent,
  ],
    imports: [
        BrowserModule,
        FormsModule,
        AppRoutingModule,
        HttpClientModule,
        StoreModule.forRoot(
            {
                customGames: customGamesReducer,
                map: mapReducer
            }),
        EffectsModule.forRoot([]),
        MatDialogActions,
        MatDialogContent,
        MatDialogClose,
        NgApexchartsModule,
        MatFormFieldModule,
        MatSelectModule,
        BrowserAnimationsModule,
        MatInput,
        MatButton,
        ToastrModule.forRoot({
            timeOut: 5000,
            positionClass: 'toast-top-right',
            preventDuplicates: false,
        }),
        MatIcon,
    ],
  providers: [
    provideAnimationsAsync('noop')
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
