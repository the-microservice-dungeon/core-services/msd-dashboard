import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import {environment} from "../../environments/environment";

@Injectable({providedIn: 'root'})
export class GamelogService {

  private baseUrl = environment.SCOREBOARD_URL || 'http://localhost:8089';
  private scoreboardEndpoint = `${this.baseUrl}/scoreboard`;
  private achievementsEndpoint = `${this.baseUrl}/achievements`;
  private mapEndpoint = `${this.baseUrl}/map`;
  private scoreboardTrophiesEndpoint = `${this.baseUrl}/scoreboard-trophies`;
  private scoreboardWithAchievementsEndpoint = `${this.baseUrl}/scoreboard-with-achievements`;

    constructor (private http : HttpClient) {}

    getScoreboard() {
        return this.http.get(this.scoreboardEndpoint);
    }

    getAchievements() {
        return this.http.get(this.achievementsEndpoint);
    }

    getMap() {
        return this.http.get(this.mapEndpoint);
    }

    getScoreboardTrophies() {
        return this.http.get(this.scoreboardTrophiesEndpoint);
    }

    getScoreboardWithAchievements() {
        return this.http.get(this.scoreboardWithAchievementsEndpoint);
    }
}
