import {Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {Game} from "../controlpanel/gameshandler/gameshandler.component";
import {Subscription, timer} from "rxjs";
import {MatchDataFetchService} from "../match-data-fetch.service";


@Component({
  selector: 'app-match-statistics',
  templateUrl: './match-statistics.component.html',
  styleUrl: './match-statistics.component.css'
})
export class MatchStatisticsComponent implements OnInit, OnDestroy{

  private gameSubscription: Subscription;
  game: Game ;
  showSecondChart = false;


  constructor(
      private matchDataFetchService: MatchDataFetchService) {
  }


  ngOnInit(): void {
    this.gameSubscription = timer(0, 1000).pipe().subscribe(() => {
      let games = this.matchDataFetchService.getGames()
      if(games.length>0){
        this.game = games[0];
      }
      else {
        this.game = null;
      }
    })
  }


  onRemoveChart(){
    this.showSecondChart = false;
  }


  onAddChart(){
    this.showSecondChart = true;
  }


  ngOnDestroy(): void {
    if(this.gameSubscription)
      this.gameSubscription.unsubscribe();
  }
}
