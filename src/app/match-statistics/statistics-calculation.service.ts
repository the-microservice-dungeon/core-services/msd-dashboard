import { Injectable } from '@angular/core';
import {Cargo, Robot} from "../map/robot/robot.component";
import {Player} from "../map/sidebar/player/player.component";

@Injectable({
  providedIn: 'root'
})
export class StatisticsCalculationService {

  constructor() { }


  calculateCurrentBalancePerRoundForPlayer(player: Player, playersByRound: {[round: number]: Player[] }): { round: number, money: number }[] {
    let balancePerRound: {round: number, money: number}[] = [];
    const indexes = Object.keys(playersByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      const playerInRound = playersByRound[round].find(p => p.playerId === player.playerId);
      if (playerInRound) {
        balancePerRound.push({ round: round, money: playerInRound.money });
      }
    })
    return balancePerRound;
  }

  calculateRobotCountPerRoundForPlayer(player: Player, robotsByRound: {[round: number]: Robot[] }): { round: number, count: number }[] {
    let robotsPerRound: {round: number, count: number}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      let robotCount = 0;
      robotsByRound[round].forEach(robot =>{
        if(robot.playerId === player.playerId){
          robotCount += 1;
        }
      })
      robotsPerRound.push({ round: round, count: robotCount })
    });
    return robotsPerRound;
  }


  calculateMoneyEarnedPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, money: number }[] {
    let moneyPerRound: {round: number, money: number}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      if (robotsByRound[round - 1] !== undefined) {
        let oldRobots = robotsByRound[round - 1]
          .filter(robot => robot.playerId === player.playerId);
        let newRobots = robotsByRound[round]
          .filter(robot => robot.playerId === player.playerId);
        let money = this.calculateMoneyEarned(oldRobots, newRobots);
        moneyPerRound.push({ round: round, money: money });
      }
    });
    return moneyPerRound;
  }


  private calculateMoneyEarned(oldRobots: Robot[], newRobots: Robot[]):number {
    let money: number = 0

    oldRobots.forEach(oldRobot => {
      let newRobot = newRobots.find(robot => robot.robotId === oldRobot.robotId)
      if (newRobot !== undefined) {
        money += this.calculateEarningsFromCargoChangeForRobot(oldRobot, newRobot)
      }
    });
    return money;
  }


  private calculateEarningsFromCargoChangeForRobot(oldRobot: Robot, newRobot: Robot) {
    const oldCargo = oldRobot.cargo;
    const newCargo = newRobot.cargo;
    let earnings = 0;
    const prices = { coal: 5, iron: 15, gem: 30, gold: 50, platin: 60 };

    for (const item in prices) {
      const soldAmount = oldCargo[item] - newCargo[item];
      if (soldAmount > 0) {
        earnings += soldAmount * prices[item];
      }
    }
    return earnings;
  }


  calculateMoneySpentPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, money: number }[] {
    let moneyForUpgrades = this.calculateMoneySpentOnUpgradesPerRoundForPlayer(player, robotsByRound);
    let moneyForRobots = this.calculateMoneySpentOnRobotsPerRoundForPlayer(player, robotsByRound);
    let moneyTotal= [];

    for (let i = 0; i < moneyForUpgrades.length; i++) {
      let round = moneyForUpgrades[i].round;
      let money = moneyForUpgrades[i].money + moneyForRobots[i].money;
      moneyTotal.push({round, money})
    }
    return moneyTotal;
  }


  calculateMoneySpentOnUpgradesPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, money: number }[] {
    let moneyPerRound: {round: number, money: number}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      if (robotsByRound[round - 1] !== undefined) {
        let oldRobots = robotsByRound[round - 1]
          .filter(robot => robot.playerId === player.playerId);
        let newRobots = robotsByRound[round]
          .filter(robot => robot.playerId === player.playerId);
        let money = this.calculateMoneySpentOnUpgrades(oldRobots, newRobots);
        moneyPerRound.push({ round: round, money: money });
      }
    });
    return moneyPerRound;
  }


  private calculateMoneySpentOnUpgrades(oldRobots: Robot[], newRobots: Robot[]):number {
    let money: number = 0

    oldRobots.forEach(oldRobot => {
      let newRobot = newRobots.find(robot => robot.robotId === oldRobot.robotId)
      if (newRobot !== undefined) {
        money += this.calculateMoneySpentOnUpgradesForRobot(oldRobot.levels, newRobot.levels)
      }
    });
    return money;
  }

  private calculateMoneySpentOnUpgradesForRobot(oldLevels, newLevels):number {
    const upgradeCosts = [0, 50, 300, 1500, 4000, 15000];
    let totalCost = 0;

    const calculateCostForLevel = (oldLevel, newLevel) => {
      let cost = 0;
      for (let level = oldLevel + 1; level <= newLevel; level++) {
        cost += upgradeCosts[level] || 0;
      }
      return cost;
    };

    totalCost += calculateCostForLevel(oldLevels.miningLevel, newLevels.miningLevel);
    totalCost += calculateCostForLevel(oldLevels.storage, newLevels.storage);
    totalCost += calculateCostForLevel(oldLevels.miningSpeed, newLevels.miningSpeed);
    totalCost += calculateCostForLevel(oldLevels.damage, newLevels.damage);
    totalCost += calculateCostForLevel(oldLevels.energyRegeneration, newLevels.energyRegeneration);
    totalCost += calculateCostForLevel(oldLevels.health, newLevels.health);
    totalCost += calculateCostForLevel(oldLevels.energy, newLevels.energy);
    return totalCost;
  }

  calculateMoneySpentOnRobotsPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, money: number }[] {
    let moneyPerRound: {round: number, money: number}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      if (robotsByRound[round - 1] !== undefined) {
        let oldRobotIds = new Set(robotsByRound[round - 1]
          .filter(robot => robot.playerId === player.playerId)
          .map(robot => robot.robotId)
        );
        let newRobotIds = new Set(robotsByRound[round]
          .filter(robot => robot.playerId === player.playerId)
          .map(robot => robot.robotId)
        );
        let money = this.calculateMoneySpentOnRobots(oldRobotIds, newRobotIds);
        moneyPerRound.push({ round: round, money: money });
      }
    });
    return moneyPerRound;
  }

  private calculateMoneySpentOnRobots(oldRobotIds: Set<string>, newRobotIds: Set<string>):number {
    let money: number = 0

    newRobotIds.forEach(newRobotId => {
      if (!oldRobotIds.has(newRobotId)) {
        money += 100;
      }
    });
    return money;
  }

  calculateDeadRobotsPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, count: number }[] {
    let deadRobotsPerRound: {round: number, count: number}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      if (robotsByRound[round - 1] !== undefined) {
        let oldRobotIds = new Set(robotsByRound[round - 1]
          .filter(robot => robot.playerId === player.playerId)
          .map(robot => robot.robotId)
        );
        let newRobotIds = new Set(robotsByRound[round]
          .filter(robot => robot.playerId === player.playerId)
          .map(robot => robot.robotId)
        );
        let deadRobots = this.calculateDeadRobots(oldRobotIds, newRobotIds);
        deadRobotsPerRound.push({ round: round, count: deadRobots });
      }
    });
    return deadRobotsPerRound;
  }

  private calculateDeadRobots(oldRobotIds: Set<string>, newRobotIds: Set<string>): number {
    let numberOfDeadRobots = 0;

    oldRobotIds.forEach(oldRobotId => {
      if (!newRobotIds.has(oldRobotId)) {
        numberOfDeadRobots += 1;
      }
    });
    return numberOfDeadRobots;
  }

  calculateResourceFarmedPerRoundForPlayer(player: Player, robotsByRound: { [round: number]: Robot[] }): { round: number, cargo: Cargo }[] {
    let resourcePerRound: {round: number, cargo: Cargo}[] = [];
    const indexes = Object.keys(robotsByRound).map(Number).sort((a, b) => a - b);

    indexes.forEach(round => {
      if (robotsByRound[round - 1] !== undefined) {
        let oldRobots = robotsByRound[round - 1]
          .filter(robot => robot.playerId === player.playerId);
        let newRobots = robotsByRound[round]
          .filter(robot => robot.playerId === player.playerId);
        let resource = this.calculateResourceFarmed(oldRobots, newRobots);
        resourcePerRound.push({ round: round, cargo: resource });
      }
    });
    return resourcePerRound;
  }

  private calculateResourceFarmed(oldRobots: Robot[], newRobots: Robot[]): Cargo {
    let resource: Cargo = {capacity: 0, used: 0, free: 0, coal: 0, iron:0, gem: 0, gold:0, platin:0};

    oldRobots.forEach(oldRobot => {
      let newRobot = newRobots.find(robot => robot.robotId === oldRobot.robotId)
      if (newRobot !== undefined) {
        if(newRobot.cargo.coal > oldRobot.cargo.coal)
          resource.coal += (newRobot.cargo.coal - oldRobot.cargo.coal)
        if(newRobot.cargo.iron > oldRobot.cargo.iron)
          resource.iron += (newRobot.cargo.iron - oldRobot.cargo.iron)
        if(newRobot.cargo.gem > oldRobot.cargo.gem)
          resource.gem += (newRobot.cargo.gem - oldRobot.cargo.gem)
        if(newRobot.cargo.gold > oldRobot.cargo.gold)
          resource.gold += (newRobot.cargo.gold - oldRobot.cargo.gold)
        if(newRobot.cargo.platin > oldRobot.cargo.platin)
          resource.platin += (newRobot.cargo.platin - oldRobot.cargo.platin)
      }
    });
    return resource;
  }


}
