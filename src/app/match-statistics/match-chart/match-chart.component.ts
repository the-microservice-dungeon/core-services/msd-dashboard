import {Component, OnDestroy, OnInit, ViewChild} from "@angular/core";
import {Game} from "../../controlpanel/gameshandler/gameshandler.component";
import {Subscription, timer} from "rxjs";
import {
  ApexAxisChartSeries,
  ApexChart,
  ApexDataLabels,
  ApexGrid,
  ApexStroke,
  ApexTitleSubtitle,
  ApexXAxis,
  ChartComponent,
  ChartType
} from "ng-apexcharts";
import {Planet} from "../../map/planet/planet.component";
import {Robot} from "../../map/robot/robot.component";
import {Player} from "../../map/sidebar/player/player.component";
import {MatchDataService} from "../../match-data.service";
import {StatisticsCalculationService} from "../statistics-calculation.service";
import {MatchDataFetchService} from "../../match-data-fetch.service";
import {ChartDataService} from "./chart-data.service";


export type ChartOptions = {
  series: ApexAxisChartSeries;
  chart: ApexChart;
  xaxis: ApexXAxis;
  dataLabels: ApexDataLabels;
  grid: ApexGrid;
  stroke: ApexStroke;
  title: ApexTitleSubtitle;
  yaxis: any;
  plotOptions: any,
  legend: any,
  colors: any
};

export type ChartLabeling = {
  name:string,
  xAxis:string,
  yAxis:string
};

@Component({
  selector: 'app-match-chart',
  templateUrl: './match-chart.component.html',
  styleUrl: './match-chart.component.css'
})
export class MatchChartComponent implements OnInit, OnDestroy{

  private dataSubscription: Subscription;
  private chartUpdateSubscription: Subscription;

  //for showing charts after game has finished (dummy-only)
  private latestGame: Game;
  robotsByRound: { [round: number]: Robot[] } = {};
  planetsByRound: { [round: number]: Planet[] } = {};
  playersByRound: { [round: number]: Player[] } = {};

  private roundLength:number = 15000;
  game: Game;

  chartLabelings: ChartLabeling[] = [
    {name: "Current Balance" , xAxis: "Round", yAxis:"Money"},
    {name: "Robot Count", xAxis: "Round", yAxis:"Number of Robots Alive"},
    {name: "Accumulated Money Spent", xAxis: "Round", yAxis:"Total Amount of Money Spent"},
    {name: "Money Spent per Round", xAxis: "Round", yAxis:"Money Spent"},
    {name: "Accumulated Amount of Money Earned", xAxis: "Round", yAxis:"Amount of Money Earned"},
    {name: "Money Earned per Round", xAxis: "Round", yAxis:"Amount of Money Earned"},
    {name: "Accumulated Number of Dead Robots" , xAxis: "Round", yAxis:"Number of Dead Robots"},
    {name: "Dead Robots per Round" , xAxis: "Round", yAxis:"Number of Dead Robots"},  //working
    {name: "Accumulated Amount of Money Spent on Upgrades" , xAxis: "Round", yAxis:"Amount of Money Spent"},
    {name: "Money Spent on Upgrades per Round" , xAxis: "Round", yAxis:"Amount of Money spent"},
    {name: "Accumulated Amount of Money Spent on Robots" , xAxis: "Round", yAxis:"Amount of Money Spent"},
    {name: "Money Spent on Robots per Round" , xAxis: "Round", yAxis:"Amount of Money spent"},
    {name: "Accumulated Amount of Coal Farmed" , xAxis: "Round", yAxis:"Amount of Coal"},
    {name: "Coal Farmed per Round" , xAxis: "Round", yAxis:"Amount of Coal"},
    {name: "Accumulated Amount of Iron Farmed" , xAxis: "Round", yAxis:"Amount of Iron"},
    {name: "Iron Farmed per Round" , xAxis: "Round", yAxis:"Amount of Iron"},
    {name: "Accumulated Amount of Gem Farmed" , xAxis: "Round", yAxis:"Amount of Gem"},
    {name: "Gem Farmed per Round" , xAxis: "Round", yAxis:"Amount of Gem"},
    {name: "Accumulated Amount of Gold Farmed" , xAxis: "Round", yAxis:"Amount of Gold"},
    {name: "Gold Farmed per Round" , xAxis: "Round", yAxis:"Amount of Gold"},
    {name: "Accumulated Amount of Platin Farmed" , xAxis: "Round", yAxis:"Amount of Platin"},
    {name: "Platin Farmed per Round" , xAxis: "Round", yAxis:"Amount of Platin"},
  ];

  categoryChartDataMethodMap: { [key: string]: () => void } = {
    "Current Balance": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateCurrentBalancePerRoundForPlayer.bind(this.statisticsCalculationService),false, null, () => this.playersByRound),
    "Robot Count": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateRobotCountPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Money Spent": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateMoneySpentPerRoundForPlayer.bind(this.statisticsCalculationService), true),
    "Money Spent per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateMoneySpentPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Amount of Money Earned":  this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateMoneyEarnedPerRoundForPlayer.bind(this.statisticsCalculationService), true),
    "Money Earned per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateMoneyEarnedPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Amount of Money Spent on Upgrades": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateMoneySpentOnUpgradesPerRoundForPlayer.bind(this.statisticsCalculationService), true),
    "Money Spent on Upgrades per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateMoneySpentOnUpgradesPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Amount of Money Spent on Robots": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateMoneySpentOnRobotsPerRoundForPlayer.bind(this.statisticsCalculationService), true),
    "Money Spent on Robots per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateMoneySpentOnRobotsPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Number of Dead Robots": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateDeadRobotsPerRoundForPlayer.bind(this.statisticsCalculationService), true),
    "Dead Robots per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateDeadRobotsPerRoundForPlayer.bind(this.statisticsCalculationService)),
    "Accumulated Amount of Coal Farmed": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), true, 'coal'),
    "Coal Farmed per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), false, 'coal'),
    "Accumulated Amount of Iron Farmed": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), true, 'iron'),
    "Iron Farmed per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), false, 'iron'),
    "Accumulated Amount of Gem Farmed": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), true, 'gem'),
    "Gem Farmed per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), false, 'gem'),
    "Accumulated Amount of Gold Farmed": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), true, 'gold'),
    "Gold Farmed per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), false, 'gold'),
    "Accumulated Amount of Platin Farmed": this.setNewChartData.bind(this,"line", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), true, 'platin'),
    "Platin Farmed per Round": this.setNewChartData.bind(this,"bar", this.statisticsCalculationService.calculateResourceFarmedPerRoundForPlayer.bind(this.statisticsCalculationService), false, 'platin'),
  };

  selectedCategory: ChartLabeling;
  @ViewChild("chart") chart: ChartComponent;
  chartHeight = 600;
  chartOptions: Partial<ChartOptions>;
  lastChartZoom = null;
  chartType: ChartType = "line";
  xAxisCategories: number[] = [];
  chartInputData: any = [];
  customColors = [
    '#008FFB', '#00E396', '#FEB019', '#FF4560', '#775DD0',
    '#3F51B5', '#546E7A', '#D4526E', '#8D5B4C', '#F86624',
    '#D7263D', '#1B998B', '#2E294E', '#F46036', '#E2C044',
    '#000000', '#69D2E7', '#C8A2C8', '#80CED7', '#006400'
  ];
  constructor(
    private matchDataFetchService: MatchDataFetchService,
    private matchDataService: MatchDataService,
    private statisticsCalculationService: StatisticsCalculationService,
    private chartDataService: ChartDataService
  ) {
    this.selectedCategory = this.chartLabelings[0];
  }

  ngOnInit(): void {
    this.latestGame = this.matchDataFetchService.getLatestGame();
    this.initializeChartOptions();

    //additional delayed chart-update to give application time to load data from database
    setTimeout(() => {
      this.changeChartInputDataBasedOnCategory();
      this.updateChart();
    }, 2000);

    this.dataSubscription = timer(0, 1000).pipe().subscribe(() => {
      let games = this.matchDataFetchService.getGames()
      if(games.length>0){
        this.game = games[0];
        this.latestGame = games[0];
        this.roundLength = this.game.roundLengthInMillis;
      }
      this.robotsByRound = this.matchDataService.getRobotsByRound();
      this.planetsByRound = this.matchDataService.getPlanetsByRound();
      this.playersByRound = this.matchDataService.getPlayersByRound();
    })

    this.chartUpdateSubscription = timer(0, this.roundLength).pipe().subscribe(() => {
      this.changeChartInputDataBasedOnCategory();
      this.updateChart();
    });
  }

  changeChartInputDataBasedOnCategory() {
    const selectedCategoryName = this.selectedCategory.name;
    const methodToCall = this.categoryChartDataMethodMap[selectedCategoryName] || this.setStandardChartData.bind(this);
    methodToCall();
  }

  setNewChartData(chartType: ChartType, dataCalculationFn, isAccumulated = false, resourceName = null, dataByRound = () => this.robotsByRound): void {
    const chartData = this.chartDataService.setChartInputData(
      dataCalculationFn,
      dataByRound(),
      isAccumulated,
      resourceName
    );
    this.chartType = chartType;
    this.chartInputData = chartData.chartInputData;
    this.xAxisCategories = chartData.xAxisCategories;
  }

  //dummy for not implemented categories
  setStandardChartData(){
    this.chartType = "line";
    if(this.latestGame.gameStatus !== 'created') {
      this.xAxisCategories = Array(this.latestGame.maxRounds).fill(0).map((_, index) => index + 1)
      this.chartInputData = [{
        name: "Dummy",
        data: Array(this.latestGame.currentRoundNumber).fill(0).map((_, index) => index + 2)
      }]
    }
  }

  initializeChartOptions(){
    this.chartOptions = {
      series: [],
      chart: {
        height: 600,
        width: '100%',
        type: this.chartType,
        zoom: {
          type: "x",
          enabled: true,
          autoScaleYaxis: true
        },
        toolbar: {
          tools:{
            zoom:false,
          },
          autoSelected: "pan",
        },
        events:{
          beforeResetZoom: () => {
            this.lastChartZoom = null;
            this.resetZoom();
          },
          zoomed: (_, value)  => {
            this.lastChartZoom = [value.xaxis.min, value.xaxis.max];
          },
          scrolled: (_, { xaxis }) => {
            this.setZoom(xaxis.min, xaxis.max)
          },
        }
      },
      colors: this.customColors,
      dataLabels: {
        enabled: false,
        offsetY: -20,
        style: {
          colors: ["#333"]
        }
      },
      stroke: {
        curve: "straight",
        width: 5,
      },
      title: {
        text: this.selectedCategory.name,
        align: "center",
        style: {
          fontSize:  '23px',
          color:  'black',
          fontFamily: 'Roboto',
        },
      },
      grid: {
        row: {
          colors: ["#f3f3f3", "transparent"],
          opacity: 0.5
        },
      },
      xaxis: {
        title:{
          text: this.selectedCategory.xAxis,
          offsetY: -10,
          style:{
            fontFamily: 'Roboto',
          }
        },
        categories: [],
        tickPlacement: 'on',
        min: null,
        max: null,
        tickAmount: 20
      },
      yaxis: {
        title: {
          text: this.selectedCategory.yAxis,
          style:{
            fontFamily: 'Roboto',
          }
        },
      },
      plotOptions: {
        bar: {
          dataLabels: {
            position: "top"
          }
        }
      },
      legend:{
        show: true,
        position: 'bottom',
        offsetY: -5,
        fontFamily: 'Roboto',
      },
    };
  }

  updateChart(){
    const newChartOptions = {
      chart: {
        type: this.chartType,
        height: this.chartHeight,
      },
      xaxis: {
        min: this.lastChartZoom?.[0] ?? null,
        max: this.lastChartZoom?.[1] ?? null,
        title:{
          text: this.selectedCategory.xAxis,
        },
        categories: this.xAxisCategories,
        tickAmount: 20,
      },
      series: this.chartInputData,
      title:{
        text: this.selectedCategory.name,
      },
      yaxis: {
        title: {
          text: this.selectedCategory.yAxis,
          style:{
            fontFamily: 'Roboto',
          }
        },
      },
      stroke: {
        width: this.getStrokeWidthByChartType(),
      },
    };
    this.chart.updateOptions(newChartOptions)
      .catch(error => {
        console.error("Error updating chart:", error);
      });
  }

  getStrokeWidthByChartType()   {
    return this.chartType === "line" ? 5 : 0.5;
  };

  onCategoryChange() {
    this.resetZoom();
    this.changeChartInputDataBasedOnCategory();
    this.updateChart();
  }

  resetZoom(){
    this.lastChartZoom = null;
    this.updateChart();
  }

  setZoom(min:number, max:number){
    this.lastChartZoom = [min, max];
  }

  onChartHeightChange(){
    this.updateChart();
  }

  ngOnDestroy(): void {
    if(this.dataSubscription)
      this.dataSubscription.unsubscribe();
    if(this.chartUpdateSubscription)
      this.chartUpdateSubscription.unsubscribe();
  }
}
