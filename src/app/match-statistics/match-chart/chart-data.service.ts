import { Injectable } from '@angular/core';
import {MatchDataFetchService} from "../../match-data-fetch.service";

@Injectable({
  providedIn: 'root'
})
export class ChartDataService {

  cargoResourceMap = {
    coal: (cargo) => cargo.coal,
    iron: (cargo) => cargo.iron,
    gem: (cargo) => cargo.gem,
    gold: (cargo) => cargo.gold,
    platin: (cargo) => cargo.platin
  };

  constructor(private matchDataFetchService: MatchDataFetchService) { }

  setChartInputData(dataCalculationFn, dataByRound: any, isAccumulated: boolean, resourceName: string) {
    let latestParticipatingPlayers = this.matchDataFetchService.getLatestParticipatingPlayers();

    const chartInputData = [];
    let xAxisCategories = [];

    const getCargoResourceValue = resourceName ? this.cargoResourceMap[resourceName] : (entry) => {
      if(entry.money !== undefined) return entry.money
      else if (entry.count !== undefined) return entry.count
      else return 0
    }

    latestParticipatingPlayers.forEach(player => {
      let data = [];
      let calculatedData = dataCalculationFn(player, dataByRound);

      //set empty, so we dont have duplicates in the array for every player in for-loop
      xAxisCategories = [];

      calculatedData.forEach(entry => {
        let value = getCargoResourceValue(entry.cargo || entry);
        if (isAccumulated) {
          let previousCount = data[data.length - 1] || 0;
          value += previousCount;
        }
        xAxisCategories.push(entry.round);
        data.push(value);
      });

      chartInputData.push({ name: player.name, data });
    });

    return {chartInputData, xAxisCategories };
  }
}
