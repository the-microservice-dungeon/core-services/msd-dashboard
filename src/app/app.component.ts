import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CustomGame } from './controlpanel/controlpanel.component';
import { Store } from '@ngrx/store';
import { init } from './store/dashboard.actions';
import { Game } from './controlpanel/gameshandler/gameshandler.component';
import {MatchDataService} from "./match-data.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements OnInit {
  ngOnInit(): void {
    this.store.dispatch(init())
  }
  constructor(private store: Store, private matchDataService: MatchDataService) {
  }
}
export function saveGamesToLocalStorage(games: Game[]) {
  localStorage.setItem('games', JSON.stringify(games));
}

export function loadGamesFromLocalStorage(): Game[] {
  const savedGames = localStorage.getItem('games');
  if (savedGames) {
    return JSON.parse(savedGames);
  }
  return []
}

export function saveCustomGamesToLocalStorage(games: CustomGame[]) {
  localStorage.setItem('customGames', JSON.stringify(games));
}

export function loadCustomGamesFromLocalStorage(): CustomGame[] {
  const savedGames = localStorage.getItem('customGames');
  if (savedGames) {
    return JSON.parse(savedGames);
  }
  return []
}

export function initializeIndexedDatabase() {
  return new Promise<IDBDatabase>((resolve, reject) => {
    const request = indexedDB.open("database", 1);

    request.onerror = function(event) {
      reject("Database error: " + (event.target as IDBRequest).error);
    };

    request.onupgradeneeded = function(event) {
      const db = (event.target as IDBOpenDBRequest).result;

      if (!db.objectStoreNames.contains("planets")) {
        const objectStore1 = db.createObjectStore("planets", { keyPath: "round"});
        objectStore1.createIndex("round", "round", { unique: true });
      }

      if (!db.objectStoreNames.contains("robots")) {
        const objectStore2 = db.createObjectStore("robots", { keyPath: "round" });
        objectStore2.createIndex("round", "round", { unique: true });
      }

      if (!db.objectStoreNames.contains("players")) {
        const objectStore3 = db.createObjectStore("players", { keyPath: "round" });
        objectStore3.createIndex("round", "round", { unique: true });
      }
    };

    request.onsuccess = function(event) {
      resolve((event.target as IDBOpenDBRequest).result);
    };
  });
}

export function addEntryToIndexedDatabase<T>(storeName: string, data: T) {
  initializeIndexedDatabase().then(db => {
    const transaction = db.transaction([storeName], "readwrite");
    const objectStore = transaction.objectStore(storeName);

    const request = objectStore.put(data);

    request.onsuccess = () => {
      //console.log("Data added or updated in store:", storeName);
    };
    request.onerror = (event:Event) => {
      console.log("Error adding or updating data in store:", storeName, (event.target as IDBRequest).error);
    };
  }).catch(error => {
    console.error("Error initializing database:", error);
  });
}

export function clearIndexedDatabase(storeName: string) {
  initializeIndexedDatabase().then(db => {
    const transaction = db.transaction([storeName], "readwrite");
    const objectStore = transaction.objectStore(storeName);

    const request = objectStore.clear();

    request.onsuccess = function(event) {
      console.log("All data cleared from store:", storeName);
    };

    request.onerror = function(event) {
      console.log("Error clearing store:", storeName, (event.target as IDBRequest).error);
    };
  }).catch(error => {
    console.error("Error initializing database:", error);
  });
}

export function deleteIndexedDatabase() {
  const deleteRequest = indexedDB.deleteDatabase("database");

  deleteRequest.onsuccess = function(event) {
    console.log("Database successfully deleted");
  };

  deleteRequest.onerror = function(event) {
    console.log("Error deleting database:", (event.target as IDBRequest).error);
  };
}

export function getAllFromIndexedDatabaseBy(storeName: string) {
  return initializeIndexedDatabase().then(db => {
    return new Promise<[]>((resolve, reject) => {
      const transaction = db.transaction([storeName], "readonly");
      const objectStore = transaction.objectStore(storeName);
      const request = objectStore.getAll();

      request.onsuccess = function(event) {
        console.log("Data loaded successfully!");
        resolve((event.target as IDBRequest).result);
      };

      request.onerror = function(event) {
        reject("Error getting all: " + (event.target as IDBRequest).error);
      };
    });
  });
}
