import { NgModule } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { ControlpanelComponent } from "./controlpanel/controlpanel.component";
import { MapComponent } from "./map/map.component";
import { ScoreboardComponent } from "./scoreboard/scoreboard.component";
import {MatchStatisticsComponent} from "./match-statistics/match-statistics.component";

const appRoutes : Routes = [
    {path: 'controlpanel', component: ControlpanelComponent},
    {path: 'map', component: MapComponent},
    {path: 'scoreboard', component: ScoreboardComponent},
    {path: 'match-statistics', component: MatchStatisticsComponent},
    {path: '', component: ControlpanelComponent},
    {path: '**', redirectTo: '', pathMatch: 'full' }
]
@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}
