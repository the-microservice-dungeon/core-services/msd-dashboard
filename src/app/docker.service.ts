import { Injectable } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import {catchError, of, throwError} from "rxjs";
import {environment} from "../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class DockerService {
  private dashboardPlayerApiEndpoint = environment.DASHBOARD_PLAYER_API_URL || 'http://localhost:3100';

  constructor(private http: HttpClient) { }

  buildDockerImage(context:string, imageName:string){
    return this.http.post(this.dashboardPlayerApiEndpoint + '/' + 'docker/build', {context: context, imageName:imageName}).pipe(
      catchError((error) => {
        console.error('Fehler beim Builden des Docker-Images:', error);
        return of(null);
      })
    );
  }

  startDockerContainerWithConfigFile(imageName: string, port: number, containerName: string, configFile: File){
    const formData = new FormData();
    formData.append('imageName', imageName);
    formData.append('port', port.toString());
    formData.append('containerName', containerName);
    formData.append('configFile', configFile);
    return this.http.post(this.dashboardPlayerApiEndpoint + '/docker/configureAndRunWithUpload', formData).pipe(
      catchError((error) => {
        console.error(`Fehler beim Starten des Docker-Containers: ${containerName}: ${error.error.message}`, error);
        return throwError(() => error);
        //return of(null);
      })
    );
  }

  stopDockerContainer(containerName: string){
    return this.http.post(this.dashboardPlayerApiEndpoint + '/' + 'docker/stop', {containerName: containerName}).pipe(
      catchError((error) => {
        console.error('Fehler beim Stoppen des Docker-Containers:', error);
        return throwError(() => error);
      })
    );
  }

  stopAndRemoveDockerContainer(containerName: string){
    return this.http.post(this.dashboardPlayerApiEndpoint + '/' + 'docker/stopAndRemove', {containerName: containerName}).pipe(
      catchError((error) => {
        console.error('Fehler beim Stoppen und Entfernen des Docker-Containers:', error);
        return throwError(() => error);
      })
    );
  }

}
