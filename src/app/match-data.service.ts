import {Injectable} from '@angular/core';
import {Planet} from "./map/planet/planet.component";
import {Robot} from "./map/robot/robot.component";
import {Player} from "./map/sidebar/player/player.component";
import {Game} from "./controlpanel/gameshandler/gameshandler.component";
import {Subscription, timer} from "rxjs";
import {MoneyService} from "./money.service";
import {SharedService} from "./shared/shared.service";
import {PlanetService} from "./planet.service";
import {RobotsService} from "./robot.service";
import {GamesService} from "./games.service";
import {PlayerService} from "./player.service";
import {JunkyardService} from "./map/sidebar/junkyard/junkyard.service";

import {
  addEntryToIndexedDatabase,
  clearIndexedDatabase,
  getAllFromIndexedDatabaseBy,
  loadGamesFromLocalStorage,
  saveGamesToLocalStorage
} from "./app.component";
import {
  MatchDataFetchService,
  PlanetDatabaseEntry,
  PlayerDatabaseEntry,
  RobotDatabaseEntry
} from "./match-data-fetch.service";

@Injectable({
  providedIn: 'root'
})
export class MatchDataService {

  planets: Planet[] = [];
  robots: Robot[] = [];
  players: Player[] = [];
  games: Game[];
  showPlanetInfo = true;
  errorUrl: string = "";
  mapScale: number = 1.0;
  backgroundImageType = "nothing"
  private dataLoaded: boolean;

  private intervalTime = 5000;
  private oldRobots: Robot[];

  private matchDataSubscription:Subscription;
  private gameStatusSubscription: Subscription;
  private subscriptionsStarted = false;

  private planetsByRound: { [round: number]: Planet[] } = {};
  private robotsByRound: { [round: number]: Robot[] } = {};
  private playersByRound: { [round: number]: Player[] } = {};


  constructor(
    private moneyService: MoneyService,
    private sharedService: SharedService,
    private planetService: PlanetService,
    private robotService: RobotsService,
    private gamesService: GamesService,
    private playerService: PlayerService,
    private junkyardService: JunkyardService,
    private matchDataFetchService: MatchDataFetchService,
    ) {
    console.log("started loading data");
    this.dataLoaded = false;
    this.loadAllData().then(() => {
      this.dataLoaded = true;
      console.log("finished loading data");
    }).catch(error => {
      console.error("Error loading initial data:", error);
    });

    this.gameStatusSubscription = timer(0, 1000).pipe().subscribe(() => this.checkGamestatus());
  }

  private checkGamestatus(){
    this.games = this.matchDataFetchService.getGames();
    if(this.games.length>0){
      this.intervalTime = this.games[0].roundLengthInMillis;
      if(this.games[0].gameStatus === 'started'){
        if(!this.subscriptionsStarted){
          this.startSubscriptions();
          this.subscriptionsStarted = true;
        }
      }
      else{
        this.stopSubscriptions();
        this.resetAll();
      }

    } else{
      this.stopSubscriptions();
      this.players = [];
      this.robots = [];
      this.planets = [];
    }
    saveGamesToLocalStorage(this.games);
  }

  private startSubscriptions() {
    this.games = loadGamesFromLocalStorage();


    this.matchDataSubscription = timer(0,1000).subscribe(() => {
      if(this.dataLoaded) {
        this.onGetMatchData()
      }
    });
  }

  private stopSubscriptions() {
    this.subscriptionsStarted = false;
    if(this.matchDataSubscription) {
      this.matchDataSubscription.unsubscribe();
    }
  }

  private resetAll(){
    this.planetsByRound  = {};
    this.robotsByRound = {};
    this.playersByRound = {};

    this.oldRobots = [];
    this.robots = [];
    this.planets = [];
    this.players = [];

    clearIndexedDatabase("robots");
    clearIndexedDatabase("planets");
    clearIndexedDatabase("players");
  }

  private onGetMatchData(){
    let robotsByRound = this.matchDataFetchService.getRobotsByRound();
    let planetsByRound = this.matchDataFetchService.getPlanetsByRound();

    this.oldRobots = this.getLatestEntryOrEmptyOf(this.robotsByRound);
    this.robots = this.getLatestEntryOrEmptyOf(robotsByRound);
    this.planets = this.getLatestEntryOrEmptyOf(planetsByRound);
    this.players = this.matchDataFetchService.getPlayers();

    this.updatePlanets();
    this.updatePlayers();

    const planetRound = this.getHighestIndex(planetsByRound);
    this.planetsByRound[planetRound] = JSON.parse(JSON.stringify(this.planets));
    let planetDatabaseEntry = this.createPlanetDatabaseEntryFrom(planetRound, this.planets);
    this.savePlanetEntryToIndexedDatabase(planetDatabaseEntry)

    const robotRound = this.getHighestIndex(robotsByRound);
    this.robotsByRound[robotRound] = JSON.parse(JSON.stringify(this.robots));
    let robotDatabaseEntry = this.createRobotDatabaseEntryFrom(robotRound, this.robots);
    this.saveRobotEntryToIndexedDatabase(robotDatabaseEntry);

    //this.player liefert erst ab runde 3 daten, davor funktioniert scoreboard nicht und man bekommt keine player-ids
    const playerRound = this.matchDataFetchService.getCurrentRound()
    this.playersByRound[playerRound] = JSON.parse(JSON.stringify(this.players));
    let playerDatabaseEntry = this.createPlayerDatabaseEntryFrom(playerRound, this.players);
    this.savePlayerEntryToIndexedDatabase(playerDatabaseEntry);

  }

  private getLatestEntryOrEmptyOf(object: { [round: number]: any[] }){
    const highestIndex = this.getHighestIndex(object);
    if(highestIndex > -Infinity){
      return object[highestIndex];
    }
    else{
      return [];
    }
  }

  private getHighestIndex<T>( object: { [round: number]: T[] }) {
    const indexes = Object.keys(object).map(Number);
    return Math.max(...indexes)
  }

  private calculateEarningsFromCargoChange(playerId, oldRobot, newRobot) {
    const oldCargo = oldRobot.cargo;
    const newCargo = newRobot.cargo;
    let earnings = 0;
    const prices = { coal: 5, iron: 15, gem: 30, gold: 50, platin: 60 };
    for (const item in prices) {
      const soldAmount = oldCargo[item] - newCargo[item];
      if (soldAmount > 0) {
        this.playerService.addTransaction(playerId, { type: 'SELL', entity: oldRobot.name, item: item, amount: soldAmount, earning: soldAmount * prices[item] })
        earnings += soldAmount * prices[item];
      }
    }
    this.moneyService.addMoney(playerId, earnings);
  }

  private calculateSpentOnUpgrades(robot, playerId, oldLevels, newLevels) {
    const upgradeCosts = [0, 50, 300, 1500, 4000, 15000];
    let totalCost = 0;

    const calculateCostForLevel = (oldLevel, newLevel, levelName) => {
      let cost = 0;
      for (let level = oldLevel + 1; level <= newLevel; level++) {
        cost += upgradeCosts[level] || 0;
        if (cost !== 0) {
          this.playerService.addTransaction(playerId, { type: 'BUY', entity: robot.name, item: levelName, amount: newLevel, earning: -upgradeCosts[level] })
        }
      }
      return cost;
    };

    totalCost += calculateCostForLevel(oldLevels.miningLevel, newLevels.miningLevel, 'miningLevel');
    totalCost += calculateCostForLevel(oldLevels.storage, newLevels.storage, 'storage');
    totalCost += calculateCostForLevel(oldLevels.miningSpeed, newLevels.miningSpeed, 'miningSpeed');
    totalCost += calculateCostForLevel(oldLevels.damage, newLevels.damage, 'damage');
    totalCost += calculateCostForLevel(oldLevels.energyRegeneration, newLevels.energyRegeneration, 'energyRegeneration');
    totalCost += calculateCostForLevel(oldLevels.health, newLevels.health, 'health');
    totalCost += calculateCostForLevel(oldLevels.energy, newLevels.energy, 'energy');
    this.moneyService.subtractMoney(playerId, totalCost)
  }

  private getOldRobotById(oldRobots, robotId) {
    return oldRobots.find(oldRobot => oldRobot.robotId === robotId) || null;
  }

  private updatePlayerBalances(oldRobots, newRobots) {
    let playerRobotMap = new Map<string, number>();
    newRobots.forEach(newRobot => {
      const oldRobot = this.getOldRobotById(oldRobots, newRobot.robotId);
      const playerId = newRobot.playerId;
      if (oldRobot) {
        this.calculateEarningsFromCargoChange(playerId, oldRobot, newRobot);
        this.calculateSpentOnUpgrades(newRobot, playerId, oldRobot.levels, newRobot.levels);
      } else {
        this.moneyService.subtractMoney(playerId, 100)
        const oldAmount = playerRobotMap.get(playerId) || 0;
        playerRobotMap.set(playerId, oldAmount + 1);
      }
    });

    playerRobotMap.forEach((amount, playerId) => {
      this.playerService.addTransaction(playerId, { type: 'BUY', entity: "", item: "robot", amount: amount, earning: -100 * amount });
    });

  }

  private updatePlayerMoneyFromMoneyService(){
    this.players.forEach(player => {
      player.money = this.moneyService.getMoney(player.playerId)
    })
  }
  getPlayerById(id: string): Player {
    return this.players.find(player => player.playerId === id)
  }
  hasHighlightedRobot(planet: Planet): boolean {
    for (const robot of planet.robots) {
      if (robot.highlighted) {
        return true;
      }
    }
    return false;
  }

  private updatePlanets() {
    this.planets.forEach(planet => {
      planet.highlighted = false;
      planet.robots = [];
    });

    for (const robot of this.robots) {
      const targetPlanet = this.planets.find(planet => planet.planetId === robot.planetId);

      if (targetPlanet) {
        targetPlanet.robots.push(robot);

        if (robot.highlighted) {
          targetPlanet.highlighted = true;
        }
      }
    }
  }

  private updatePlayers() {
    this.players.forEach(player => {
      this.updatePlayersVisitedPlanets(player);
      this.robots.forEach(robot => {
        if (robot.playerId === player.playerId) {
          const existingRobotIndex = player.robots.findIndex(prevRobot => prevRobot.robotId === robot.robotId);
          if (existingRobotIndex !== -1) {
            if (!this.robotService.areAttributesEqual(player.robots[existingRobotIndex], robot)) {
              Object.assign(player.robots[existingRobotIndex], robot);
            }
          } else {
            player.robots.push(robot);
          }
        }
      });
    });
    if (this.games[0].currentRoundNumber > 5) {
      this.players.forEach(player => {
        player.robots = player.robots.filter(playerRobot => {
          const isRobotActive = this.robots.some(robot => robot.robotId === playerRobot.robotId);
          if (!isRobotActive) {
            this.junkyardService.addDeadRobot(playerRobot)
          }
          return this.robots.some(robot => robot.robotId === playerRobot.robotId);
        });
      });
    }

    this.updatePlayerBalances(this.oldRobots, this.robots);
    this.updatePlayerMoneyFromMoneyService();
  }

  private updatePlayersVisitedPlanets(player:Player){
    this.robots.forEach(robot => {
      if(robot.playerId === player.playerId && !player.visitedPlanetIds.includes(robot.planetId)){
        player.visitedPlanetIds.push(robot.planetId)
      }
    })
    const oldPlayers: Player[] = this.getLatestEntryOrEmptyOf(this.playersByRound)
    oldPlayers.forEach(oldPlayer => {
      if (oldPlayer.playerId === player.playerId) {
        oldPlayer.visitedPlanetIds.forEach(visitedPlanetId => {
          if (!player.visitedPlanetIds.includes(visitedPlanetId))
            player.visitedPlanetIds.push(visitedPlanetId);
        })
      }
    })
  }

  getRobotsByRound(){
    return this.robotsByRound;
  }

  getPlanetsByRound(){
    return this.planetsByRound;
  }

  getPlayersByRound(){
    return this.playersByRound;
  }

  getPlayers(){
    return this.players;
  }

  private createPlanetDatabaseEntryFrom(round: number, planets: Planet[]){
    return {round:round, planets:planets};
  }

  private createRobotDatabaseEntryFrom(round: number, robots: Robot[]){
    return {round:round, robots:robots};
  }

  private createPlayerDatabaseEntryFrom(round: number, players: Player[]){
    return {round:round, players:players};
  }

  private async saveRobotEntryToIndexedDatabase(entry: RobotDatabaseEntry){
    try {
      addEntryToIndexedDatabase("robots", entry);
    } catch (error) {
      console.error("Error saving robot entry to database:", error);
    }
  }

  private async savePlanetEntryToIndexedDatabase(entry: PlanetDatabaseEntry){
    try {
      addEntryToIndexedDatabase("planets", entry);
    } catch (error) {
      console.error("Error saving planet entry to database:", error);
    }
  }

  private async savePlayerEntryToIndexedDatabase(entry: PlayerDatabaseEntry){
    try {
      addEntryToIndexedDatabase("players", entry);
    } catch (error) {
      console.error("Error saving player entry to database:", error);
    }
  }

  async loadAllData() {
    await Promise.all([
      this.loadRobotsFromIndexedDatabase(),
      this.loadPlanetsFromIndexedDatabase(),
      this.loadPlayersFromIndexedDatabase()
    ]);
  }

  private async loadRobotsFromIndexedDatabase(): Promise<void> {
    try {
      const data: RobotDatabaseEntry[] = await getAllFromIndexedDatabaseBy("robots");
      this.robotsByRound = this.mapDatabaseEntriesToRobots(data);
    } catch (error) {
      console.error("Error loading Robot-Data:", error);
    }
  }

  private async loadPlanetsFromIndexedDatabase(): Promise<void> {
    try {
      const data: PlanetDatabaseEntry[] = await getAllFromIndexedDatabaseBy("planets");
      this.planetsByRound = this.mapDatabaseEntriesToPlanets(data);
    } catch (error) {
      console.error("Error loading Planet-Data:", error);
    }
  }

  private async loadPlayersFromIndexedDatabase(): Promise<void> {
    try {
      const data: PlayerDatabaseEntry[] = await getAllFromIndexedDatabaseBy("players");
      this.playersByRound = this.mapDatabaseEntriesToPlayers(data);
    } catch (error) {
      console.error("Error loading Player-Data:", error);
    }
  }

  private mapDatabaseEntriesToPlanets(planetDatabaseEntries: PlanetDatabaseEntry[]){
    let planetsByRound: { [round: number]: Planet[] } = {};
    planetDatabaseEntries.forEach(entry =>{
      planetsByRound[entry.round] = entry.planets;
    })
    return planetsByRound;
  }

  private mapDatabaseEntriesToRobots(robotDatabaseEntries: RobotDatabaseEntry[]){
    let robotsByRound: { [round: number]: Robot[] } = {};
    robotDatabaseEntries.forEach(entry =>{
      robotsByRound[entry.round] = entry.robots;
    })
    return robotsByRound;
  }

  private mapDatabaseEntriesToPlayers(playerDatabaseEntries: PlayerDatabaseEntry[]){
    let playersByRound: { [round: number]: Player[] } = {};
    playerDatabaseEntries.forEach(entry =>{
      playersByRound[entry.round] = entry.players;
    })
    return playersByRound;
  }
}
