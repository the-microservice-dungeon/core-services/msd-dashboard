import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {BehaviorSubject, catchError, map, Observable, of, tap} from "rxjs";
import {DockerService} from "./docker.service";
import {environment} from "../environments/environment";


export interface CustomPlayer  {
  name: string,
  email: string,
  port: number,
  imageName:string,
  configFile: any,
  gameId:string
}

@Injectable({
  providedIn: 'root'
})
export class CustomPlayerService {

  private pathToPlayerImageData: string = '../assets/playerconfigs/playerimages.json';
  private currentCustomPlayers: CustomPlayer[] = [];
  private standardPort:number = 8100;
  private GAME_HOST_CUSTOM_PLAYER:string = 'http://game:8080';
  private RABBITMQ_HOST_CUSTOM_PLAYER:string = 'rabbitmq';
  private configFileName:string = 'playerConfig.json';
  private standardConfigFileName:string = 'standardConfig.json';

  private _playerImageData: BehaviorSubject< { [key: string]: string }> = new BehaviorSubject< { [key: string]: string }>({});
  public playerImageData$: Observable< { [key: string]: string }> = this._playerImageData.asObservable();


  constructor(private http: HttpClient,
              private dockerService: DockerService) {
    this.loadPlayerImageData();
    this.currentCustomPlayers = this.getCurrentCustomPlayersFromLocalStorage();
  }


  loadPlayerImageData(){
    this.http.get<{ [key: string]: string }>(this.pathToPlayerImageData).pipe(
      map(data => {
        this._playerImageData.next(data);
      }),
      catchError(error => {
        console.error('Error loading Json-Data of standard players', error);
        return of({});
      })
    ).subscribe();
  }

  getCurrentCustomPlayersFromLocalStorage(): CustomPlayer[]{
    const key = "currentCustomPlayers"
    const data = localStorage.getItem(key);
    return data ? JSON.parse(data) : [];
  }

  saveCurrentCustomPlayersToLocalStorage(){
    const key = "currentCustomPlayers";
    const data = this.currentCustomPlayers;
    localStorage.setItem(key,JSON.stringify(data));
  }

  getCurrentCustomPlayer(): CustomPlayer[]{
    return this.currentCustomPlayers
  }

  async createCustomPlayerBy(playerName: string, image: string, configFile: any, gameId:string): Promise<CustomPlayer> {
    try {
      const name = this.generateCustomNameBy(playerName);
      const email = this.generateCustomEmailBy(playerName);
      const port = this.generateFreePort();
      let editedFile: any;

      if (configFile) {
        editedFile = await this.editConfigFile(name, email, configFile);
      } else {
        editedFile = await this.loadAndEditStandardConfigFile(playerName, name, email);
      }
      const newPlayer: CustomPlayer = {
        name: name,
        email: email,
        port: port,
        imageName: image,
        configFile: editedFile,
        gameId:gameId
      };
      this.currentCustomPlayers.push(newPlayer);
      this.saveCurrentCustomPlayersToLocalStorage();
      return newPlayer;
    }
    catch (error){
      throw error;
    }
  }

  generateCustomNameBy(playerName:string): string{
    let botNumber = this.currentCustomPlayers.length +1
    return "BOT" + botNumber + "_" + playerName;
  }

  generateCustomEmailBy(playerName:string): string{
    let botNumber = this.currentCustomPlayers.length +1
    return "BOT" + botNumber + "_" + playerName +"@bot.com";
  }

  generateFreePort():number{
    if(this.currentCustomPlayers.length == 0){
      return this.standardPort;
    }else{
      const playerWithHighestPort = this.currentCustomPlayers.reduce((prev, current) => {
        return (prev.port > current.port) ? prev : current;
      });
      return playerWithHighestPort.port+1;
    }
  }

  editConfigFile(playerName:string, playerEmail:string, configFile:any){
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (e) => {
        try {
          let data = e.target.result as string;
          const editedData = this.editJsonConfigFileAttributesFrom(data, playerName, playerEmail);
          const editedFile = new File([editedData], this.configFileName, { type: 'text/plain' });
          resolve(editedFile);
        } catch (error) {
          console.error('Error editing the config-file:', error);
          reject(error);
        }
      };
      reader.readAsText(configFile);
    });
  }

  //reminder: it is actually not required to load the standard config file, it could just be an empty file or
  // not loading a file at all, since we would create the file from scratch anyways. This should be changed in the future
  loadAndEditStandardConfigFile(imageName:string, playerName:string, playerEmail:string) {
    return new Promise((resolve, reject) => {
      const pathToStandardPlayerConfig = `../assets/playerconfigs/${this.standardConfigFileName}`;
      this.http.get(pathToStandardPlayerConfig, { responseType: 'text' })
        .pipe(
          catchError(error => {
            console.error('Error loading standard config-file:', error);
            reject(error);
            return of(null);
          })
        )
        .subscribe({
          next: (data) => {
            const editedData = this.editJsonConfigFileAttributesFrom(data, playerName, playerEmail);
            const editedFile = new File([editedData], this.configFileName, { type: 'text/plain' });
            resolve(editedFile);
          },
          error: (err) => {
            console.error('Error in load+edit the config-file:', err);
            reject(err);
          }
        });
    });
  }


  editJsonConfigFileAttributesFrom(data:string, playerName:string, playerEmail:string):string{
    try {
      const config = JSON.parse(data);
      config['GAME_HOST'] = this.GAME_HOST_CUSTOM_PLAYER;
      config['RABBITMQ_HOST'] = this.RABBITMQ_HOST_CUSTOM_PLAYER;
      config['PLAYER_NAME'] = playerName;
      config['PLAYER_EMAIL'] = playerEmail;

      //GAME_URL, da Hackschnitzel einen anderen Namen für game-service benutzt als die anderen
      config['GAME_URL'] = this.GAME_HOST_CUSTOM_PLAYER

      //diese 3 Parameter braucht player 'thelegend27', sonst funktioniert der nicht, deshalb gebe ich sie mit an, obwohl die immer gleich bleiben
      config['RABBITMQ_USERNAME'] = "admin";
      config['RABBITMQ_PASSWORD'] = "admin";
      config['RABBITMQ_PORT'] = "5672";
      return JSON.stringify(config, null, 2);
    }
    catch (error){
      throw error;
    }
  }

  deleteCustomPlayerByName(playerName:string){
    this.currentCustomPlayers = this.currentCustomPlayers.filter(player => player.name != playerName);
    this.saveCurrentCustomPlayersToLocalStorage();
  }

  deleteAllCustomPlayerByGameId(gameId:string){
    for (let i = this.currentCustomPlayers.length - 1; i >= 0; i--) {
      if (this.currentCustomPlayers[i].gameId === gameId) {
        console.log("Deleting: "+ this.currentCustomPlayers[i].name)
        this.currentCustomPlayers.splice(i, 1);
      }
    }
    this.saveCurrentCustomPlayersToLocalStorage();
  }

  clearAll(){
    this.currentCustomPlayers = [];
    this.saveCurrentCustomPlayersToLocalStorage();
  }

  stopAllCustomPlayerContainersByGameId(gameId: string){
    this.getCurrentCustomPlayer().filter(customPlayer => customPlayer.gameId == gameId).forEach(customPlayer => {
      this.dockerService.stopAndRemoveDockerContainer(customPlayer.name)
        .pipe(
          tap(res => {
            console.log(`Docker-Container ${customPlayer.name} stopped and deleted successfully.`);
          }),
          catchError(error => {
            console.error(`Error stopping and deleting the Docker-Container ${customPlayer.name}:`, error);
            return of(null);
          })
        ).subscribe()
    });
    this.deleteAllCustomPlayerByGameId(gameId)
  }

  getImageNameByKey(imageKey:string){
    return this._playerImageData.getValue()[imageKey];
  }

}
