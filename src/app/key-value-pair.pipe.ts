import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'keyValuePair'
})
export class KeyValuePairPipe implements PipeTransform {

  transform(value: any, key: string): string {
    return value[key];
  }

}
