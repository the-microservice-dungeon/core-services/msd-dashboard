import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ResourceType, Robot } from '../robot/robot.component';
import { Subscription } from 'rxjs';
import { SharedService } from '../../shared/shared.service';
import { PlayerService } from '../../player.service';
import {MatchDataService} from "../../match-data.service";

export type Planet = {
  planetId: string;
  resourceType: ResourceType | null;
  resource: Resource | null;
  movementDifficulty: number;
  position: { x: number; y: number } | null;
  robots: Robot[],
  highlighted: boolean
}

export type Resource = {
  amount: number;
  capacity: number;
}

@Component({
  selector: 'app-planet',
  templateUrl: './planet.component.html',
  styleUrl: './planet.component.css'
})
export class PlanetComponent implements OnInit, OnDestroy {
  @Input() planet: Planet;
  @Input() showInfo: boolean;
  @Input() selectedGridPlayer: string;

  displayedRobots: Robot[];


  robotsLoading: Record<string, boolean> = {};
  copiedClass = '';
  robotImageType: string;
  backgroundImageType: string;
  robotScale = 1.0;
  materialImageScale = 1.0;
  materialType = 'image'
  planetInformation = [true, true, true]
  planetStatus = ""
  showAllRobotsForSelectedPlayerGrid: boolean = true;

  private robotImageTypeSubscription: Subscription;
  private backgroundImageTypeSubscription: Subscription;
  private materialImageScaleSubscription: Subscription;
  private robotScaleSubscription: Subscription;
  private materialTypeSubscription: Subscription;
  private planetInformationSubscription: Subscription;
  private showOnlyRobotsForSelectedPlayerGridSubscription: Subscription
  constructor(
    private sharedService: SharedService,
    private playerService: PlayerService,
    private matchDataService: MatchDataService) { }

  ngOnInit() {
    this.planet.robots.forEach(robot => {
      this.robotsLoading[robot.robotId] = true;
    });
    this.robotImageTypeSubscription = this.sharedService.robotImageType.subscribe(type => this.robotImageType = type);
    this.backgroundImageTypeSubscription = this.sharedService.backgroundImage.subscribe(type => this.backgroundImageType = type);
    this.robotScaleSubscription = this.sharedService.robotScale.subscribe(scale => this.robotScale = scale)
    this.materialImageScaleSubscription = this.sharedService.materialImageScale.subscribe(scale => this.materialImageScale = scale)
    this.materialTypeSubscription = this.sharedService.materialType.subscribe(type => this.materialType = type)
    this.planetInformationSubscription = this.sharedService.planetInformation.subscribe(tuple => this.planetInformation = tuple)
    this.showOnlyRobotsForSelectedPlayerGridSubscription = this.sharedService.showAllRobotsForSelectedPlayerGrid.subscribe(selected => this.showAllRobotsForSelectedPlayerGrid = selected)
  }
  onRobotImageLoad(robotId: string) {
    this.robotsLoading[robotId] = false;
  }
  getPlayerColorOfRobot(robot: Robot) {
    return this.playerService.getPlayerColor(robot.playerId)
  }

  calculatePlanetStatus(): string {
    const fighting = new Set<string>();
    let atLeastOneRobotDamaged = false;

    for (const robot of this.planet.robots) {
      fighting.add(robot.playerId);
      if (robot.vitals.health < 10) {
        atLeastOneRobotDamaged = true;
      }
    }
    if (fighting.size > 1 && atLeastOneRobotDamaged) {
      return 'fighting';
    }
    const mining = this.planet.robots.some(robot => {
      return robot.cargo.used > 0;
    });

    if (mining) {
      return 'mining';
    }
    return 'peaceful';
  }

  calculateRobotProportions(): string {
    const playerColors = this.playerService.getPlayerColorMap()
    let robots = this.planet.robots
    if(!this.showAllRobotsForSelectedPlayerGrid){
      robots = this.getDisplayedRobots();
    }
    const totalCount = robots.length;
    const countPerPlayer = new Map<string, number>();

    robots.forEach(robot => {
      const playerId = robot.playerId;
      const currentCount = countPerPlayer.get(playerId) || 0;
      countPerPlayer.set(playerId, currentCount + 1);
    });

    let gradientString = '';
    let accumulatedPercentage = 0;

    countPerPlayer.forEach((count, playerId) => {
      const playerPercentage = (count / totalCount) * 100;
      const color = playerColors.get(playerId) || 'transparent';
      gradientString += `${color} ${accumulatedPercentage}% ${accumulatedPercentage + playerPercentage}%, `;
      accumulatedPercentage += playerPercentage;
    });

    return gradientString.slice(0, -2);
  }

  getResourceName(resourceType: ResourceType) {
    switch (resourceType) {
      case ResourceType.COAL:
        return "Coal"
      case ResourceType.IRON:
        return "Iron"
      case ResourceType.GEM:
        return "Gem"
      case ResourceType.GOLD:
        return "Gold"
      case ResourceType.PLATIN:
        return "Platin"
      default:
        return 'nothing'
    }
  }
  getTeamRobotImage(color: string) {
    /*switch (color) {
      case 'black':
        return '../../../assets/images/robots/black-robot.png'
      case 'blue':
        return '../../../assets/images/robots/blue-robot.png'
      case 'green':
        return '../../../assets/images/robots/green-robot.png'
      case 'grey':
        return '../../../assets/images/robots/gray-robot.png'
      case 'orange':
        return '../../../assets/images/robots/orange-robot.png'
      case 'purple':
        return '../../../assets/images/robots/purple-robot.png'
      case 'red':
        return '../../../assets/images/robots/red-robot.png'
      case 'silver':
        return '../../../assets/images/robots/silver-robot.png'
      case 'yellow':
        return '../../../assets/images/robots/yellow-robot.png'
      case 'pink':
        return '../../../assets/images/robots/pink-robot.png'
      case 'neon':
        return '../../../assets/images/robots/aqua-robot.png'
      case 'white':
        return '../../../assets/images/robots/white-robot.png'
      default:
        return ""
    }*/

    const imageName = `${color.toLowerCase()}-robot.png`;
    return `../../../assets/images/robots/${imageName}`;
  }

  getBackgroundImage(type: any): string {
    if (this.backgroundImageType === 'landscape') {
      switch (type) {
        case ResourceType.COAL:
          return `url('../../../assets/images/landscapes/coal.png')`
        case ResourceType.IRON:
          return `url('../../../assets/images/landscapes/iron.png')`
        case ResourceType.GEM:
          return `url('../../../assets/images/landscapes/gem.png')`
        case ResourceType.GOLD:
          return `url('../../../assets/images/landscapes/gold.png')`
        case ResourceType.PLATIN:
          return `url('../../../assets/images/landscapes/platin.png')`
        default:
          return `url('../../../assets/images/landscapes/nothing.png')`
      }
    } else if (this.backgroundImageType === 'planet') {
      switch (type) {
        case ResourceType.COAL:
          return `url('../../../assets/images/planets/coal.png')`
        case ResourceType.IRON:
          return `url('../../../assets/images/planets/iron.png')`
        case ResourceType.GEM:
          return `url('../../../assets/images/planets/gem.png')`
        case ResourceType.GOLD:
          return `url('../../../assets/images/planets/gold.png')`
        case ResourceType.PLATIN:
          return `url('../../../assets/images/planets/platin.png')`
        default:
          return `url('../../../assets/images/planets/nothing.png')`
      }
    }
    return ""
  }


  //filterAndReturnRobotsForSelectedPlayerGrid() {
  getDisplayedRobots(){
    if (this.selectedGridPlayer === "all" || this.showAllRobotsForSelectedPlayerGrid === true) {
      return this.planet.robots;
    } else {
      const selectedPlayer = this.matchDataService.getPlayers().find(player => player.name === this.selectedGridPlayer);
      if (!selectedPlayer) {
        console.warn(`Couldn't filter for selected playergrid because, player with name ${this.selectedGridPlayer} not found.`);
        return this.planet.robots;
      }
      const selectedGridPlayerId = selectedPlayer.playerId;
      return this.planet.robots.filter(robot => robot.playerId === selectedGridPlayerId);
    }
  }


  allRobotsLoaded(): boolean {
    return Object.values(this.robotsLoading).every(status => !status);
  }

  formatNumber(num: number): string {
    if (num >= 1000) {
      return (Math.floor(num / 100) / 10).toFixed(1) + 'k';
    } else {
      return num.toString();
    }
  }
  async copyToClipboard(value: string) {
    try {
      await navigator.clipboard.writeText(value);
      this.copiedClass = 'visible';
      setTimeout(() => this.copiedClass = '', 2000);
    } catch (err) {
      console.error('Failed to copy: ', err);
    }
  }

  ngOnDestroy() {
    if (this.backgroundImageTypeSubscription) {
      this.backgroundImageTypeSubscription.unsubscribe();
    }
    if (this.robotImageTypeSubscription) {
      this.robotImageTypeSubscription.unsubscribe();
    }
    if (this.materialImageScaleSubscription) {
      this.materialImageScaleSubscription.unsubscribe();
    }
    if (this.robotScaleSubscription) {
      this.robotScaleSubscription.unsubscribe();
    }
    if (this.materialTypeSubscription){
      this.materialTypeSubscription.unsubscribe()
    }
    if (this.planetInformationSubscription){
      this.planetInformationSubscription.unsubscribe()
    }
    if (this.showOnlyRobotsForSelectedPlayerGridSubscription){
      this.showOnlyRobotsForSelectedPlayerGridSubscription.unsubscribe()
    }
  }
}
