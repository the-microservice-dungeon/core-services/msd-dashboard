import { Injectable } from '@angular/core';
import { Robot } from '../../robot/robot.component';

@Injectable({
  providedIn: 'root'
})
export class JunkyardService {

  private deadRobots : Robot[] = []

  constructor() { }

  addDeadRobot(robot: Robot){
    this.deadRobots.push(robot)
  }

  getDeadRobots():Robot[]{
    return this.deadRobots;
  }

  clear(){
    this.deadRobots = []
  }
}
