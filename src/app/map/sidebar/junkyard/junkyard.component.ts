import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Robot } from '../../robot/robot.component';
import { JunkyardService } from './junkyard.service';
import { Subscription, interval } from 'rxjs';
import { Player } from '../player/player.component';

@Component({
  selector: 'app-junkyard',
  templateUrl: './junkyard.component.html',
  styleUrl: './junkyard.component.css'
})
export class JunkyardComponent implements OnInit, OnDestroy {

  @Input() players : Player[];
  
  deadRobotsSubscription: Subscription


  deadRobots: Robot[] = [];

  constructor(private junkyardService: JunkyardService) {

  }

  ngOnInit(): void {
    this.deadRobotsSubscription = interval(5000).subscribe(() => {
      this.deadRobots = this.junkyardService.getDeadRobots();
    })
  }

  ngOnDestroy(){
    if (this.deadRobotsSubscription){
      this.deadRobotsSubscription.unsubscribe()
    }
  }
}
