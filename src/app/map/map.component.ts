import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { PlanetService } from '../planet.service';
import { Subscription, interval, switchMap, takeWhile } from 'rxjs';
import { RobotsService } from '../robot.service';
import { GamesService } from '../games.service';
import { PlayerService } from '../player.service';
import { Player } from './sidebar/player/player.component';
import { Robot } from './robot/robot.component';
import { Planet } from './planet/planet.component';
import { Router } from '@angular/router';
import { loadGamesFromLocalStorage, saveGamesToLocalStorage } from '../app.component';
import { Game } from '../controlpanel/gameshandler/gameshandler.component';
import { Store } from '@ngrx/store';
import { SharedService } from '../shared/shared.service';
import { MoneyService } from '../money.service';
import { JunkyardService } from './sidebar/junkyard/junkyard.service';
import {CustomPlayerService} from "../custom-player.service";
import {MatchDataService} from "../match-data.service";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit, OnDestroy {

  planets: Planet[] = [];
  robots: Robot[] = [];
  players: Player[] = [];
  games: Game[];
  grid: (Planet | null)[][] = [];
  undiscoveredPlanets: Planet[] = [];
  fetching = true;
  showPlanetInfo = true;
  errorUrl: string = "";
  mapScale: number = 1.0;
  backgroundImageType = "nothing"

  private intervalTime = 2000;
  private oldRobots: Robot[];

  private backgroundImageSubscription: Subscription;
  private backgroundSubscription: Subscription;
  private planetSubscription: Subscription;
  private robotSubscription: Subscription;
  private gamesSubscription: Subscription;
  private playerSubscription: Subscription;
  private junkyardSubscription: Subscription;

  selectedGridPlayer: string = 'all';


  constructor(
    private moneyService: MoneyService,
    private sharedService: SharedService,
    private planetService: PlanetService,
    private robotService: RobotsService,
    private gamesService: GamesService,
    private playerService: PlayerService,
    private junkyardService: JunkyardService,
    private matchDataService: MatchDataService,
    private customPlayerService: CustomPlayerService,
    private router: Router,
    private store: Store<{ robot: Robot[], player: Player[], }>) { }

  ngOnInit() {
    this.games = loadGamesFromLocalStorage();
    this.planetSubscription = interval(this.intervalTime).subscribe(() => this.loadPlanetData());
    this.robotSubscription = interval(this.intervalTime).subscribe(() => this.loadRobotData());
    //this.pollForPlayers();
    this.playerSubscription = interval(this.intervalTime).subscribe(() => this.loadPlayerData());
    this.gamesSubscription = interval(this.intervalTime).subscribe(() => this.loadGameData());
    document.addEventListener('DOMContentLoaded', () => {
      this.backgroundSubscription = this.sharedService.backgroundColor.subscribe(color => {
        const mapContainer = document.getElementById('map-container');
        if (mapContainer) {
          mapContainer.style.backgroundColor = color;
        }
      });
    });
    this.backgroundImageSubscription = this.sharedService.backgroundImage.subscribe(type => this.backgroundImageType = type)
    this.sharedService.mapScale.subscribe(scale => {
      this.mapScale = scale;
    });
    this.junkyardSubscription = interval(this.intervalTime).subscribe(() => this.junkyardService.getDeadRobots)
  }

  getBGimage() {
    if (this.backgroundImageType === "landscape") {
      return `url('../assets/images/landscapes/void.png')`
    } else if (this.backgroundImageType === "planet") {
      return `url('../assets/images/planets/void.png')`
    }
    return ""
  }

  getUndiscoveredBGimage(){
    if (this.backgroundImageType === "landscape") {
      return `url('../assets/images/landscapes/undiscovered.png')`
    } else if (this.backgroundImageType === "planet") {
      return `url('../assets/images/planets/undiscovered.png')`
    }
    return ""
  }

  determineTextColor(){
    if (this.backgroundImageType === "planet") {
      return 'white';
    }
    else {
      return 'black';
    }
  }

  handleMouseMove(event: MouseEvent, imageId: string) {
    const img = document.getElementById(imageId) as HTMLImageElement;

    const { width, height, left, top } = img.getBoundingClientRect();
    const centerX = left + width / 2;
    const centerY = top + height / 2;
    const mouseX = event.clientX;
    const mouseY = event.clientY;
    const deltaX = (mouseX - centerX) / width;
    const deltaY = (mouseY - centerY) / height;

    const tiltX = deltaY * 20;
    const tiltY = deltaX * -20;

    img.style.transform = `rotateX(${tiltX}deg) rotateY(${tiltY}deg) scale(1.05)`;
  }

  resetTilt(imageId: string) {
    const img = document.getElementById(imageId) as HTMLImageElement;
    img.style.transform = 'none';
  }

  private getPosition(planet: Planet): { x: number; y: number } {
    return planet.position || { x: 0, y: 0 };
  }

  private loadRobotData() {
    this.robots = this.matchDataService.robots;
  }

  private loadPlanetData() {
    this.planets = this.matchDataService.planets;
    this.setupGrid();
    this.fetching = false;
  }

  private loadPlayerData(){
    this.players = this.matchDataService.players;
  }
  trackByRowIndex(index: number, row: any): number {
    return index;
  }
  trackByPlanetId(index: number, planet: Planet | null): string | null {
    return planet ? planet.planetId : null;
  }

  private loadGameData() {
      this.games = this.matchDataService.games
      if (this.games) {
        if (this.games.length > 0) {
          this.intervalTime = this.games[0].roundLengthInMillis;
        }
      }
      if (this.games.length === 0) {
        this.players = [];
        this.robots = [];
        this.planets = [];
        this.undiscoveredPlanets = [];
      }
  }

  onChangeGrid(name:string){
    this.selectedGridPlayer = name;
    this.loadPlanetData();
  }

  isPlanetUndiscovered(planet: Planet) {
    return this.undiscoveredPlanets.find(undiscoveredPlanet => undiscoveredPlanet.planetId === planet.planetId) !== undefined
  }

  redirectToControlpanel() {
    this.router.navigate(['/controlpanel']);
  }

  private setupGrid() {
    let maxX = Math.max(...this.planets.map(p => this.getPosition(p).x));
    let maxY = Math.max(...this.planets.map(p => this.getPosition(p).y));

    for (let i = 0; i <= maxX; i++) {
      this.grid[i] = [];
      for (let j = 0; j <= maxY; j++) {
        this.grid[i][j] = null;
      }
    }

    //this.switchPlanetsAccordingToSelectedPlayerGrid();
    this.updateUndiscoveredPlanets();
    for (let planet of this.planets) {
      const position = this.getPosition(planet);
      this.grid[position.y][position.x] = planet;
    }
  }

  updateUndiscoveredPlanets(){
    this.undiscoveredPlanets = [];
    if(this.selectedGridPlayer !== 'all'){
      let selectedPlayer = this.players.find(player => player.name === this.selectedGridPlayer);
      if (selectedPlayer) {
        this.planets.forEach(planet => {
          if(!selectedPlayer.visitedPlanetIds.includes(planet.planetId)) {
            this.undiscoveredPlanets.push(planet);
          }
        });
      }
    }
  }

  ngOnDestroy() {
    if (this.robotSubscription) {
      this.robotSubscription.unsubscribe();
    }
    if (this.planetSubscription) {
      this.planetSubscription.unsubscribe();
    }
    if (this.gamesSubscription) {
      this.gamesSubscription.unsubscribe();
    }
    if (this.playerSubscription) {
      this.playerSubscription.unsubscribe();
    }
    if (this.backgroundSubscription) {
      this.backgroundSubscription.unsubscribe();
    }
    this.backgroundImageSubscription.unsubscribe();
    saveGamesToLocalStorage(this.games);
  }
}

