import { Injectable } from "@angular/core";
import {TransactionEntry} from "./player.service";

@Injectable({
    providedIn: 'root'
})
export class MoneyService {
    private moneyMap = new Map<string, number>();

    constructor() {
      this.loadMoneyMapFromLocalStorage();
    }

    setMoney(playerId: string, amount: number): void {
        this.moneyMap.set(playerId, amount);
        this.saveMoneyMapToLocalStorage();
    }

    getMoney(playerId: string): number {
        return this.moneyMap.get(playerId) ?? -1;
    }

    addMoney(playerId: string, amount: number): void {
        const currentAmount = this.moneyMap.get(playerId) || 0;
        this.moneyMap.set(playerId, currentAmount + amount);
        this.saveMoneyMapToLocalStorage();
    }

    subtractMoney(playerId: string, amount: number): void {
        const currentAmount = this.moneyMap.get(playerId) || 0;
        if (currentAmount >= amount) {
            this.moneyMap.set(playerId, currentAmount - amount);
        } else {
            //this.moneyMap.set(playerId, 0);
          this.moneyMap.set(playerId, currentAmount - amount);
        }
        this.saveMoneyMapToLocalStorage();
    }

    getAllMoney(): Map<string, number> {
        return this.moneyMap;
    }

    clear(){
        this.moneyMap = new Map<string, number>();
        this.saveMoneyMapToLocalStorage();
    }

  private saveMoneyMapToLocalStorage(): void {
    const serializedMoneyMap = JSON.stringify(Array.from(this.moneyMap.entries()));
    localStorage.setItem('moneyMap', serializedMoneyMap);
  }

  private loadMoneyMapFromLocalStorage(): void {
    const serializedMoneyMap = localStorage.getItem('moneyMap');
    if (serializedMoneyMap) {
      const moneyMap = new Map<string, number>(JSON.parse(serializedMoneyMap));
      this.moneyMap = moneyMap;
    }
  }
}
