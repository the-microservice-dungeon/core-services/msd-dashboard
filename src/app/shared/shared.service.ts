import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({ providedIn: 'root' })
export class SharedService {
    private backgroundColorSource = new BehaviorSubject<string>('#ffffff');
    backgroundColor = this.backgroundColorSource.asObservable();
    private mapScaleSource = new BehaviorSubject<number>(1);
    mapScale = this.mapScaleSource.asObservable();
    private  robotScaleSource = new BehaviorSubject<number>(1);
    robotScale = this.robotScaleSource.asObservable();
    private  materialImageScaleSource = new BehaviorSubject<number>(1);
    materialImageScale = this.materialImageScaleSource.asObservable();
    private backgroundImageSource = new BehaviorSubject<string>('nothing');
    backgroundImage = this.backgroundImageSource.asObservable();
    private robotImageTypeSource = new BehaviorSubject<string>('random');
    robotImageType = this.robotImageTypeSource.asObservable();
    private materialTypeSource = new BehaviorSubject<string>('image');
    materialType = this.materialTypeSource.asObservable();
    private planetInformationSource = new BehaviorSubject<[boolean, boolean, boolean]>([true, true, true]);
    planetInformation = this.planetInformationSource.asObservable();
    private showAllRobotsForSelectedPlayerGridSource = new BehaviorSubject<boolean>(true);
    showAllRobotsForSelectedPlayerGrid = this.showAllRobotsForSelectedPlayerGridSource.asObservable();
    constructor() {
    }
    setBackgroundImage(type: string) {
        this.backgroundImageSource.next(type);
    }
    setMapScale(scale: number) {
        this.mapScaleSource.next(scale);
    }
    setRobotScale(scale: number){
        this.robotScaleSource.next(scale);
    }
    changeBackgroundColor(color: string) {
        this.backgroundColorSource.next(color);
    }
    setMaterialScale(scale: number){
        this.materialImageScaleSource.next(scale);
    }
    setRobotImageType(type: string) {
        this.robotImageTypeSource.next(type);
    }
    setMaterialInfo(type: string){
        this.materialTypeSource.next(type);
    }
    setPlanetInformation(tuple: [boolean,boolean,boolean]){
        this.planetInformationSource.next(tuple)
    }

    setShowAllRobotsForSelectedPlayerGrid(selected: boolean) {
      this.showAllRobotsForSelectedPlayerGridSource.next(selected);
    }
}
