export const environment = {
  GAME_HOST: 'http://game-service.game:8080',
  DASHBOARD_BACKEND_URL: 'http://localhost:8096',
  SCOREBOARD_URL: 'http://localhost:8089',
  DASHBOARD_PLAYER_API_URL: '',
  IS_KUBERNETES: true
};
