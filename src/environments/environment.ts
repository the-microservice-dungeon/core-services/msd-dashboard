export const environment = {
  GAME_HOST: (window as any).__env.GAME_HOST || 'http://localhost:8080',
  DASHBOARD_BACKEND_URL: (window as any).__env.DASHBOARD_BACKEND_URL || 'http://localhost:8096',
  SCOREBOARD_URL: (window as any).__env.SCOREBOARD_URL || 'http://localhost:8089',
  DASHBOARD_PLAYER_API_URL: (window as any).__env.DASHBOARD_PLAYER_API_URL || 'http://localhost:3100',
  IS_KUBERNETES: (window as any).__env.IS_KUBERNETES || false,
};
