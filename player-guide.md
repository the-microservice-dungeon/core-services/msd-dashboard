# How to Play Against Your Own Player Using the MSD-Dashboard?

## Overview
The Dashboard is a tool that offers real-time monitoring through a live map displaying robots, resources, and detailed information about participating players and planets. Additionally, it presents statistics both graphically and textually, some of which remain accessible even after the game ends for comprehensive analysis.
Moreover you can add different players as enemies to the game, including your own player.

To be able to use the Dashboard, you simply need the 
- [local development environment](https://the-microservice-dungeon.gitlab.io/docs/docs/getting-started/setting_up_local_devenv/) 

running on your machine.

If you use the local development environment, the dashboard will be available at localhost:4200. It will navigate you to the 'Control Panel' tab. Here, you can:

- Create a game
- Customize it
- Add players
- Start the game

After starting a game, you will be automatically navigated to the map. It takes a few rounds (usually until round 3) to finish loading. From there, you can start exploring the application and manually stop the game if needed. 

## How to Play Against Your Own Player

The Dashboard allows you to compete against your own player or other custom players. Here’s how it works:

- It creates a Docker container from the Docker image of the player on your local machine.
- It overrides all important variables (e.g., player name, player email, game service URL).
- The player runs in the Docker container and joins the game automatically.
- You can add more than one instance of a specific player to your game.

### Requirements

To play against your own player, your player needs to fulfill certain requirements.

#### 1. Docker Image

You must provide the Docker image of your player. You can do this by either:

- Adding it to our microservice-dungeon registry: registry.gitlab.com/the-microservice-dungeon/devops-team/msd-image-registry
- Having the image on your local machine or any other registry.

#### 2. Environment Variables

Your player must read and set certain variables from environment variables. This is important because the dashboard needs to change the values of certain variables to start the player correctly as a container. The following environment/system variables need to be implemented in your player with the exact same names:

```
PLAYER_NAME
PLAYER_EMAIL
GAME_HOST
RABBITMQ_HOST
```

**Important:** Please make sure to name these exactly as written here.

Other not required, but potentially necessary variables in some cases:

```
RABBITMQ_USERNAME
RABBITMQ_PASSWORD
RABBITMQ_PORT
```

### Adding Your Player to the Game

After fulfilling the requirements, visit the dashboard interface at localhost:4200 and start a game via the interface. The following steps explain how to add players to the game.

1. Open Menu:

- Click the 'Add Custom Player' button.
- Click the 'Select Own Player' button. A menu will open where you must enter the details of the player you want to add.

 <div style="text-align: left; margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/addCustomPlayer.png" style="margin-right: 10px; height: 50px; width: auto" alt="Blue button">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/selectOwnPlayer.png" style="height: 50px; width: auto" alt="Light grey button">
 </div>

2. Enter Image Registry:

- Insert the registry of your image if it is in one.
- The default input is the microservice-dungeon registry. If your player is registered there, you don’t need to change anything in this line.
- If the Docker image of your player is on your local machine, leave the input field empty.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/registry.png" style="height: 50px; width: auto" alt="Text box with an default entry and description text above">
 </div>

3. Enter Image Name:

- Insert the name of your Docker image. If the image is in the microservice-dungeon registry, the name is usually something like player-hackschnitzel.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/imageName.png" style="height: 50px; width: auto" alt="Empty text box with a description text above">
 </div>

4. Enter Image Tag:

- Insert the tag of the Docker image. The default input is latest, so you can leave it as is unless you want the image with a specific tag.

 <div style="text-align: left;margin-bottom: 30px;">
   <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/imageTag.png" style="height: 50px; width: auto" alt="Text box with an default entry and description text above">
 </div>

5. Provide Port:

- Provide a port to map the container port to the same port on the host machine (port:port).
- Leaving the field empty or set to 0 will result in a random port assignment (this should be fixed in the future in the Docker API to avoid port assignment when no value is provided).

 <div style="text-align: left;margin-bottom: 30px;">
  <img src="https://gitlab.com/the-microservice-dungeon/core-services/dashboard/msd-dashboard/raw/main/src/assets/images/readme/port.png" style="height: 50px; width: auto" alt="Empty numeric input field with a description above">
 </div>

#### Adding a Configuration to Your Player

After entering the details of your player image, the Dashboard will ask if you want to add configurations. This allows you to pass additional environment/system variables to your player for further customization. For example, you could have an environment variable named 'STRATEGY' to change the strategy of your player based on the given input. This allows you to start your player with different strategies. If you don’t have any configurations to add, just press 'No, continue without'.

If you decide to add a configuration, a file picker will open. The file you select must be a text file with a single JSON object in it. The file name is not important. It could look like this:

```
{
  "STRATEGY": "aggressive",
  "port": 43553,
  "MAX_NUMBER_ROBOTS": 100
}
```

## Playing Against Standard Players

For this feature, you don't need any special requirements. You can simply add one or more of the standard players to your game. Just press the 'Add Custom Player' button and then click on their name. Standard players cannot be configured.

**Important:** It might take some time to pull the Docker images for the first time.
